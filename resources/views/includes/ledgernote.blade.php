
            <div class="w-full text-left mb-2">
                <em class="text-gray-300">{{ $historicalNote->note }}</em> 
                ~  {{ $historicalNote->name }}, {{ date('m/d/Y', strtotime($historicalNote->created_at)) }}
            </div>