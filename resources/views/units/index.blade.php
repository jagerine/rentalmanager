@extends('layouts.app', ['heading' => 'Rental Units'])

@section('content')

    <x-utilitymenu :items="[
        ['text' => 'Add Unit', 'url' => route('units.create'), 'icon' => 'plus-circle'] 
    ]" />
    
    <div class="flex flex-col-reverse lg:flex-row mb-20">
        <div class="w-full lg:w-8/12 text-xs">
            <x-UnitLeaseSummary />  
            
        </div>  
        <div class="w-full lg:w-4/12">
            <div id="VacancyChart" class="h-56"></div>

            @include('includes.chartjs')
            
            <script type="text/javascript">
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);
                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                        ['Status', 'Total'],
                        ["{{ (int) $statusCounts['Occupied'] }} Leased", {{ (int) $statusCounts['Occupied'] }}],
                        ["{{ (int) $statusCounts['Vacant'] }} Vacant", {{ (int) $statusCounts['Vacant'] }}] 
                    ]);
          
                  // Set chart options
                  var options = {
                    is3D: true, 
                    title: 'Occupancy',
                    backgroundColor: { fill:'transparent' }, 
                    legend: { textStyle: {color: '#ddd', fontSize: 14}}, 
                    titleTextStyle: { color: '#fff', fontSize: 18},
                    vAxis: {
                        textStyle: {
                            color: '#666666',
                            fontSize: 14
                        }
                    },
                    slices: {  
                        1: {offset: 0.2}
                    }, 
                    colors: ['#badda2', '#ffcaca'], 
                  };
          
                  // Instantiate and draw our chart, passing in some options.
                  var chart = new google.visualization.PieChart(document.getElementById('VacancyChart'));
                  chart.draw(data, options);
                }
                $(window).resize(function(){
                    drawChart();
                }); 

              </script>
        </div>
    </div>

@endsection
