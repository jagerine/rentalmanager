@extends('layouts.app', ['header' => $heading])

@section('content')
    
    @if(isset($rentalUnit))
        {{ Form::model($rentalUnit, ['url' => ['units/update/'. $rentalUnit->id]]) }}
    @else
        {{ Form::open(['url' => 'units/store']) }}
    @endif

    <span class="w-full lg:w-2/6 inline-block">
        <x-forminput :params="[
            'label' => 'Nickname',
            'type' => 'text', 
            'name' => 'name'
        ]" />
            
        
    </span>

    <hr class="m-2 border-gray-700">
    

    <div x-data="{ AdvancedOptions: false }" class="text-xs text-gray-400 cursor-pointer uppercase hover:text-gray-500">
        <i :class="AdvancedOptions ? 'fas fa-arrow-circle-down' : 'fas fa-arrow-circle-right'"></i>
        <span 
            @click="AdvancedOptions = !AdvancedOptions" 
            :aria-expanded="AdvancedOptions ? 'true' : 'false'" 
            :class="{ 'active': AdvancedOptions }"
            x-text="AdvancedOptions ? 'Hide Advanced' : 'Show Advanced'">
            
            Show Advanced</span>

        <div id="AdvancedOptions" x-show="AdvancedOptions" class="mt-4">

            <div class="flex flex-col md:flex-row">
                <div class="w-full md:w-72">
                    <x-forminput :params="['name' => ' address', 'label' => 'Address']" /> 
                </div>
                <div class="w-full md:w-32">
                    <x-forminput :params="['name' => ' unit', 'label' => 'Unit']" /> 
                </div>
            </div>
            
            <div class="flex flex-col md:flex-row">
                <div class="">
                    <x-forminput :params="['name' => ' city', 'label' => 'City']" /> 
                </div>
                <div class="w-full md:w-16">
                    <x-forminput :params="['name' => ' state', 'label' => 'ST', 'maxlength' => 2]" /> 
                </div>
                <div class="">
                    <x-forminput :params="['name' => ' zip', 'label' => 'Zip']" /> 
                </div>
            </div>

            <hr class="m-2 border-gray-700">

            <div class="w-full lg:w-2/6">
                
            </div>
        </div>
    </div>

    <x-submitbutton />

        {{ Form::close() }}

@endsection