@extends('layouts.app', ['header' => $heading])

@section('content')
    
    @if(isset($rentalUnit))
        {{ Form::model($rentalUnit, ['url' => ['units/vacate/'. $rentalUnit->id]]) }}
    @else
        {{ Form::open(['url' => 'units/store']) }}
    @endif

    <h3 class="text-2xl">{{ $rentalUnit->name }}</h3>

    <div class="w-full lg:w-2/6">
            
        <div class="">
            <x-forminput :params="['type' => 'date', 'name' => 'term_date', 'label' => 'Termination Date']" /> 
        </div>
        
    </div>

    <x-submitbutton />

    {{ Form::close() }}

@endsection