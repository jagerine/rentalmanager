@extends('layouts.app', ['heading' => $rentalUnit->name])

@section('content')

    <x-utilitymenu :items="$menuOptions" />

<div class="border-t border-gray-700 border-dotted">

        <dl>        
            <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b border-gray-700 border-dotted">
                <dt class="text-sm font-medium text-gray-300">
                    Status
                </dt>
                <dd class="mt-1 text-sm sm:mt-0 sm:col-span-2 mb-4 md:mb-0">
                    {{ $rentalUnit->status }}
                </dd>
                <dt class="text-sm font-medium text-gray-300">
                    Address
                </dt>
                
                <dd class="mt-1 text-sm sm:mt-0 sm:col-span-2 mb-4 md:mb-0">
                    
                    @if( !empty($rentalUnit->address) || !empty($rentalUnit->city))

                    <address>
                        {{ $rentalUnit->address }} {{ $rentalUnit->unit }} <br>
                        {{ $rentalUnit->city }}, {{ $rentalUnit->state }} {{ $rentalUnit->zip }} 
                        @if(!empty($rentalUnit->address->street))
                            <x-maplink :search="urlencode($rentalUnit->address->street.' '.$rentalUnit->address->zip)" />
                        @endif
                    </address>  

                    @endif
                </dd>
            </div>

        </dl>
    </div>

    <x-subheading text="Lease / Tenant History" />
    
    <div class="flex">
        <div class="w-full lg:w-7/12">
           
        </div>
        <div class="w-full lg:w-4/12">
            
        </div>
    </div>

@endsection
