@extends('layouts.app', ['heading' => $title ?? 'Add Note'])

@section('content')
<div class="w-full md:6/12 lg:w-5/12">
    {{ Form::open(['url' => route('notes.save-new', [$objectType])]) }}

    <x-forminput :params="['name' => 'note', 'label' => 'Note', 'type' => 'longtext']" />

    <x-submitbutton />
    <input type="hidden" name="objectId" value="{{ $objectId }}" />
        
    {{ Form::close() }}
</div>
@endsection