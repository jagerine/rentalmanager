    

  @php
    if(!isset($data['options'])){
      $data['options'] = [];
    }
  @endphp
            <table class="table-auto divide-y divide-gray-700 w-full">
              
                @if ($data['headings'])
  
                  <thead class="">
                    <tr class="bg-gray-900">
                      <th class="py-2 px-3 text-gray-200 text-left"></th> 
                      @foreach ($data['headings'] as $heading)
  
                      <th class="py-2 px-3 text-gray-200 text-left">{{ $heading }}</th> 
                      @endforeach
  
                    </tr>
                  </thead>
                  @endif  
  
                  <tbody>
                    @if (empty($data['rows']))
                      <tr>
                        <td></td>
                        <td><em class="text-gray-400">None</em></td>
                      </tr>
                    @endif
  
                      
                      @foreach ($data['rows'] as $values)
                        
                      <tr class="border-b border-gray-700">
                          <td class="dotmenutd w-1">
                            @if (!empty($values['_actions']))
                            <x-actionmenu :items="$values['_actions']" />
                            @endif 
                          </td>
                          @foreach ($values as $valueKey => $value)
                            @if ($valueKey == '_actions' || $valueKey == '_links') 
                              @continue 
                            @endif
                            
                          <td class="px-3 py-3" data-key="{{ $valueKey }}">
                            @if ($valueKey == 'created_at' || $valueKey == 'updated_at')
                            
                              <span
                                x-data="{date: new Date($el.innerText)}"
                                x-text="date.toLocaleString().toLowerCase()"
                                >{{ $value }}</span> 
                            @else
                              @if (!empty($values['_links'][$valueKey]))
                                <a href="{{ $values['_links'][$valueKey] }}" class="page-link">
                              @endif

                              @if(isset($data['options'][$valueKey]['escape']) && $data['options'][$valueKey]['escape'] == false)
                              {!! $value !!}
                              @else
                              {{ $value }}
                              @endif
                              @if (!empty($values['_links'][$valueKey]))
                                </a>
                              @endif
                            @endif
  
                          </td>
                          @endforeach
  
                      </tr>
                      @endforeach
                      
                  </tbody>
              </table>
   