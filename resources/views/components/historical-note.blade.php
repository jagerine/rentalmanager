<div class="historical-note mb-8" id="HistoricalNote_{{ $historicalNote->id }}">
    <div class="ml-2 text-gray-500 text-xs">
        <i class="fas fa-comment text-gray-500 text-xs mr-2"></i>
        from {{ $historicalNote->createdBy()->name }} on
        {{ Timezone::convertToLocal($historicalNote->created_at, config('app.datetime_format')) }}
    </div>

    <div class="ml-7 mb-2 mr-2 italic note-body text-sm">
        {{ $historicalNote->note }}
    </div>
</div>