@props(['active'])

@php
$underLineColors = ['yellow', 'red', 'blue', 'purple','pink', 'indigo'];
shuffle($underLineColors);  
$classes = ($active ?? false)
            ? 'block items-center py-1 md:py-3 border-b-2 border-green-300 text-sm font-medium leading-5 text-green-300'
            : 'inline-flex items-center py-1 md:py-3 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-300 hover:border-'.$underLineColors[0].'-300';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
