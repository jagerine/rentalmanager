
<div class="relative pb-2 pt-2 mr-1">

    @if (!empty($params['label'])) 
    
    <label for={{ $params['name'] }} class="pl-1 text-xs text-gray-400 block">{{ $params['label'] }}</label>
    @endif
    
    @switch($params['type'])
        @case('date')
        {{ Form::text($params['name'], $params['default'] ?? null, ['data-type' => 'date', 'placeholder' => $params['placeholder'], 'class' => 'bg-gray-600 border-gray-900 rounded-lg focus:border-gray-500 w-full md:min-w-1/4']) }}
            @break
        @case('select')
            {{ Form::select($params['name'], $params['options'], $params['default'] ?? null , ['placeholder' => '', 'class'=> 'bg-gray-600 border-gray-9 00 rounded-lg focus:border-gray-500 w-full md:min-w-1/4']) }}
            @break  
        @case('checkbox')
            {{ Form::checkbox($params['name'], 1, $params['default'] ?? null, ['class'=> 'bg-gray-600 border-gray-900 rounded-lg focus:border-gray-500']) }}
            @break    
        @case('textarea')
        @case('longtext')
            {{ Form::textArea($params['name'], $params['default'] ?? null, ['placeholder' => $params['placeholder'], 'class' => 'h-32 bg-gray-600 border-gray-9   00 rounded-lg focus:border-gray-500 w-full']) }}
            @break
        @case('text')
        @default
            {{ Form::text($params['name'], $params['default'] ?? null, ['placeholder' => $params['placeholder'], 'class' => 'bg-gray-600 border-gray-900 rounded-lg focus:border-gray-500 w-full md:min-w-1/4']) }}
    @endswitch


</div>