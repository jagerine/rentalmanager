
    <div x-data="{dropdownMenu: false}" class="action-menu relative" 
    @open-dropdown.window="if ($event.detail.id == 1) dropdownMenu = true"
        @click.away="dropdownMenu = false">
    
    <button @click="dropdownMenu = ! dropdownMenu" class="ml-2">
        <span class="text-xs text-gray-200 bg-gray-500 rounded-full p-0 w-6 h-6 mr-2 inline-block flex items-center justify-center">
            <i class="fas fa-caret-down text-lg"></i>
        </span> 
    </button>
    <div x-show="dropdownMenu" class="absolute left-3 top-7 py-z mt-2 bg-gray-200 rounded-md shadow-md w-44 z-10">
        @foreach ($items as $item)
            @if(!empty($item['divider']))
    
            <div class="border-b border-gray-300 my-2 h-1"></div>

            @else

            <a 
                @if (stripos($item['text'] , 'delete') !== false) 
                    onclick="return confirm('Are you sure you want to delete this?')"
                @endif
                href="{{ $item['url'] }}" 
                class="block px-4 py-2 text-xs text-gray-300 text-gray-700 rounded-md hover:bg-gray-300 hover:text-gray-800">
                
                @if ($item['icon'])
                    <i class="fas fa-{{ $item['icon'] }} mr-2 w-3 inline-block"></i>
                @endif

                {{ $item['text'] }}
            </a>
            
            @endif

        @endforeach

    </div>
</div> 