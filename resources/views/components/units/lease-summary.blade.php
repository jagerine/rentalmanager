<div class="w-full md:w-6/12 lg:w-5/12">
    <x-forminput :params="['type' => 'text','name' => 'quickSearchInput', 'label' => false, 'placeholder' => 'Find']" /> 
</div>
<div id="UnitSummaryTable">    
    <x-datatable :data="compact('rows', 'headings','options')" />
    <script src="{{ asset('js/quickFilter.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('td[data-key=name], td[data-key=tenant]').addClass('quickSearchable');
            $('td:contains(Unpaid)').addClass('text-red-500 font-bold');
            $('input[name=quickSearchInput]').quickFilter({
                searchArea: '#UnitSummaryTable table'
            });
        });

    </script>
</div>