<div class="utility-menu mb-4 border-0  border-gray-100 pb-4 pt-2 md:pt-0">

    @foreach ($items as $item)
    
    <span class=" py-1 px-3 rounded-2xl text-gray-200 bg-black mr-1 text-sm hover:bg-gray-900 hover:text-green-600">
        <a href="{{ $item['url'] }}" id="{{ $item['id'] ?? '' }}" class="text-xs">
            <i class="fas fa-{{ $item['icon'] }} mr-1"></i>
            {{ $item['text'] }}
        </a>
    </span>

    @endforeach

</div>