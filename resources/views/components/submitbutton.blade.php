
    <div class="border-t mt-5 border-gray-200 pr-56 pt-3 inline-block mb-2">
        {{ Form::submit($text, ['name' => 'submit', 'class' => 'inline-flex justify-center py-2 px-4 border border-gray-700 shadow-sm text-sm font-medium rounded-3xl text-white bg-gray-900 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 cursor-pointer']) }}
    
    </div>