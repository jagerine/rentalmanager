@extends('layouts.app', ['heading' => 'New Ledger Entry'])

@section('content')

    @if(!$leaseId)
    
        {{ Form::open(['url' => route('ledger.add'), 'method' => 'GET']) }}

            <div class="w-full md:w-6/12 lg:w-4/12">
                <x-forminput :params="['type' => 'select','name' => 'lease', 'options' => $leases]" /> 
                <x-submitbutton text="Go" />
            </div>

        {{ Form::close() }}

    @else
        
        <div>
            <span class="text-gray-300 inline-block w-48">Lease:</span>
            <strong><a href="/tenants/lease/{{ $lease->id }}" class="page-link">{{ $lease->display_name }}</a></strong><br>
            <span class="text-gray-300 inline-block w-48">Unit:</span>
            <strong>{{ $lease->rentalUnit()->name }}</strong><br>
            <span class="text-gray-300 inline-block w-48">Rent:</span>
            <strong>${{ $lease->rent }} / {{ $lease->per }}</strong><br>
            <span class="text-gray-300 inline-block w-48">Payment Status:</span>
            <strong>{{ $lease->payment_status }}</strong><br>
        </div>

        @if(!empty($ledgerEntry))

        {{ Form::model($ledgerEntry, ['url' => '/ledger/edit/'.$ledgerEntry->id, 'method' => 'POST']) }}

        @else

        {{ Form::open(['url' => route('ledger.add'), 'method' => 'POST']) }}
            
        @endif 

            <input type="hidden" name="lease" value="{{ $leaseId }}" />
            <input type="hidden" name="add_ledger_entry" value="1" />

            <div class="flex gap-2 flex-col md:flex-row">
                <div class="w-full md:w-56">
                    <x-forminput :params="['name' => 'type', 'type'=> 'select', 'label' => 'Type', 'options' => ['Credit' => 'Credit','Debt' => 'Debt']]" /> 
                </div>
                <div class="w-full md:w-56">
                    <x-forminput :params="['name' => 'category', 'type'=> 'select', 'label' => 'Category', 'options' => ['Rent' => 'Rent', 'Utilities' => 'Utilities', 'Deposit' => 'Deposit']]" /> 
                </div>
            </div>
            <div class="flex gap-2 flex-col md:flex-row">
                <div class="w-full md:w-48">
                    <x-forminput :params="['type' => 'date','name' => 'applied_date', 'label' => 'Date']" />
                </div>
                <div class="w-full md:w-48">
                    <x-forminput :params="['type' => 'text','name' => 'amount', 'label' => 'Amount']" /> 
                </div>
            </div>
            <div class="flex gap-2 flex-col md:flex-row">
                <div class="w-full md:w-5/12">
                    <x-forminput :params="['type' => 'text','name' => 'description', 'label' => 'Description']" />     
                </div>
            </div>

            <x-submitbutton />
                
        {{ Form::close() }}

    @endif 

</div>

@endsection