@extends('layouts.app', ['heading' => 'Add New Lease'])

@section('content')
    
    @if(isset($lease))
        {{ Form::model($lease, ['url' => ['tenants/update/'. $lease->id]]) }}
    @else
        {{ Form::open(['url' => 'tenants/store']) }}
    @endif

    <span class="w-full lg:w-2/6 inline-block">

        <x-forminput :params="[
            'label' => 'Rental Unit',
            'type' => 'select', 
            'name' => 'Lease[rental_unit_id]', 
            'options' => $rentalUnits,
            'default' => $unitId
        ]" />
            
        
    </span>

    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-48">
            <x-forminput :params="['name' => 'Lease[deposit]', 'label' => 'Deposit']" /> 
        </div>
        <div class="w-full md:w-48">
            <x-forminput :params="['name' => 'Lease[rent]', 'label' => 'Rent']" /> 
        </div>
        <div class="w-full md:w-56">
            <x-forminput :params="['name' => 'Lease[per]', 'default' => 'month', 'type'=> 'select', 'label' => 'Per', 'options' => ['month' => 'month', 'week' => 'week', 'day' => 'day']]" /> 
        </div>
    </div>

    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-56">
            <x-forminput :params="['name' => 'Lease[start_date]', 'label' => 'Start Date', 'type' => 'date']" /> 
        </div>
        <div class="w-full md:w-56">
            <x-forminput :params="['name' => 'Lease[end_date]', 'label' => 'End Date', 'type' => 'date']" /> 
        </div>
        <div class="w-full md:w-64 pl-2">
            <label>
                &nbsp;
            </label><br>
            <label class="text-gray-400">
                <input type="checkbox" name="Lease[mtm_continuous]" value="1" id="MtmContinuous" checked="checked" />
                Month-to-month thereafter
            </label>
        </div>
    </div>

    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-48">
            <x-forminput :params="['name' => 'Lease[due_on]', 'default' => 'First', 'label' => 'Due On', 'type' => 'select', 'options' => array_merge(['First' => 'First'], range(2,30), ['Last' => 'Last']) ]" /> 
        </div>
    </div>

    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-6/12">
            <x-forminput :params="['name' => 'Lease[description]', 'label' => 'Notes', 'type' => 'longtext' ]" /> 
        </div>
    </div>

    <hr class="m-2 border-gray-700">

    <x-subheading text="Tenant(s)" />

    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-56">
            <x-forminput :params="['name' => 'Tenant[0][first_name]', 'label' => 'First Name', 'type' => 'text']" />
        </div>
        <div class="w-full md:w-56">
            <x-forminput :params="['name' => 'Tenant[0][last_name]', 'label' => 'Last Name', 'type' => 'text']" />
        </div>
    </div>
    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-64">
            <x-forminput :params="['name' => 'Tenant[0][email]', 'label' => 'Email', 'type' => 'email']" />
        </div>
        <div class="w-full md:w-64">
            <x-forminput :params="['name' => 'Tenant[0][phone]', 'label' => 'Phone', 'type' => 'phone']" />
        </div>
        <div class="w-full md:w-64">
            <x-forminput :params="['name' => 'Tenant[0][alt_phone]', 'label' => 'Alt Phone', 'type' => 'phone']" />
        </div>
    </div>
    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-96">
            <x-forminput :params="['name' => 'Tenant[0][description]', 'label' => 'Notes', 'type' => 'longtext']" />
        </div>
        <div class="w-full md:w-96">
            <x-forminput :params="['name' => 'Tenant[0][emergency_contact]', 'label' => 'Emergency Contact', 'type' => 'longtext']" />
        </div>
    </div>
    
    <div x-data="{ AdvancedOptions: false }" class="text-xs text-gray-400 cursor-pointer uppercase hover:text-gray-500">
        <i :class="AdvancedOptions ? 'fas fa-arrow-circle-down' : 'fas fa-arrow-circle-right'"></i>
        <span 
            @click="AdvancedOptions = !AdvancedOptions" 
            :aria-expanded="AdvancedOptions ? 'true' : 'false'" 
            :class="{ 'active': AdvancedOptions }"
            x-text="AdvancedOptions ? 'Hide Alt Address' : 'Use Alt Address'">
            
            Show Advanced</span>

        <div id="AdvancedOptions" x-show="AdvancedOptions" class="mt-4">

            <div class="flex flex-col md:flex-row">
                <div class="w-full md:w-72">
                    <x-forminput :params="['name' => 'Tenant[0][address]', 'label' => 'Address']" /> 
                </div>
                <div class="w-full md:w-32">
                    <x-forminput :params="['name' => 'Tenant[0][unit]', 'label' => 'Unit']" /> 
                </div>
            </div>
            
            <div class="flex flex-col md:flex-row">
                <div class="">
                    <x-forminput :params="['name' => 'Tenant[0][city]', 'label' => 'City']" /> 
                </div>
                <div class="w-full md:w-16">
                    <x-forminput :params="['name' => 'Tenant[0][state]', 'label' => 'ST', 'maxlength' => 2]" /> 
                </div>
                <div class="">
                    <x-forminput :params="['name' => 'Tenant[0][zip]', 'label' => 'Zip']" /> 
                </div>
            </div>

            
            
        </div>
    </div>

    <div id="TenantEntryContainer"></div>

    <div class="w-full mt-5 mb-4">
        <button class="text-xl" id="AddTenantButton">
            <i class="fas fa-plus"></i>
            Add Tenant</button>
    </div>
    <x-submitbutton />

    {{ Form::close() }}
    <div class="mb-16"></div>

    <div class="display-none invisible" id="TenantEntryTemplate" style="display: none;">
        <strong class="block mt-4">Co-Tenant</strong><button class="text-xl" id="RemoveTenantButton"><i class="fas fa-minus"></i></button>
        <div class="flex flex-col md:flex-row">
            <div class="w-full md:w-56">
                <x-forminput :params="['name' => 'Tenant[0][first_name]', 'label' => 'First Name', 'type' => 'text']" />    
            </div>
            <div class="w-full md:w-56">
                <x-forminput :params="['name' => 'Tenant[0][last_name]', 'label' => 'Last Name', 'type' => 'text']" />
            </div>
        </div>
        <div class="flex flex-col md:flex-row">
            <div class="w-full md:w-64">
                <x-forminput :params="['name' => 'Tenant[0][email]', 'label' => 'Email', 'type' => 'email']" />
            </div>
            <div class="w-full md:w-64">
                <x-forminput :params="['name' => 'Tenant[0][phone]', 'label' => 'Phone', 'type' => 'phone']" />
            </div>
            <div class="w-full md:w-64">
                <x-forminput :params="['name' => 'Tenant[0][alt_phone]', 'label' => 'Alt Phone', 'type' => 'phone']" />
            </div>
        </div>
        <div class="flex flex-col md:flex-row">
            <div class="w-full md:w-96">
                <x-forminput :params="['name' => 'Tenant[0][description]', 'label' => 'Notes', 'type' => 'longtext']" />
            </div>
            <div class="w-full md:w-96">
                <x-forminput :params="['name' => 'Tenant[0][emergency_contact]', 'label' => 'Emergency Contact', 'type' => 'longtext']" />
            </div>
        </div>
        
        <div x-data="{ AdvancedOptions: false }" class="text-xs text-gray-400 cursor-pointer uppercase hover:text-gray-500">
            <i :class="AdvancedOptions ? 'fas fa-arrow-circle-down' : 'fas fa-arrow-circle-right'"></i>
            <span 
                @click="AdvancedOptions = !AdvancedOptions" 
                :aria-expanded="AdvancedOptions ? 'true' : 'false'" 
                :class="{ 'active': AdvancedOptions }"
                x-text="AdvancedOptions ? 'Hide Alt Address' : 'Use Alt Address'">
                
                Show Advanced</span>
    
            <div id="AdvancedOptions" x-show="AdvancedOptions" class="mt-4">
    
                <div class="flex flex-col md:flex-row">
                    <div class="w-full md:w-72">
                        <x-forminput :params="['name' => 'Tenant[0][address]', 'label' => 'Address']" /> 
                    </div>
                    <div class="w-full md:w-32">
                        <x-forminput :params="['name' => 'Tenant[0][unit]', 'label' => 'Unit']" /> 
                    </div>
                </div>
                
                <div class="flex flex-col md:flex-row">
                    <div class="">
                        <x-forminput :params="['name' => 'Tenant[0][city]', 'label' => 'City']" /> 
                    </div>
                    <div class="w-full md:w-16">
                        <x-forminput :params="['name' => 'Tenant[0][state]', 'label' => 'ST', 'maxlength' => 2]" /> 
                    </div>
                    <div class="">
                        <x-forminput :params="['name' => 'Tenant[0][zip]', 'label' => 'Zip']" /> 
                    </div>
                </div>
    
                
            </div>
        </div>
    </div>

    

    <script>
        var TENANT_COUNT = 1;
        $(document).ready(function() {
            $('#AddTenantButton').click(function(e) {
                e.preventDefault();
                let template = $('#TenantEntryTemplate').html();
                $('#TenantEntryContainer').append('<div id="TenantEntry_' + TENANT_COUNT + '">'+template+'</div>');
                $('#TenantEntry_' + TENANT_COUNT).find('input, textarea').each(function() {
                    let name = $(this).attr('name');
                    $(this).attr('name', name.replace('[0]', '[' + TENANT_COUNT + ']'));
                });
                $('#TenantEntry_' + TENANT_COUNT).find('label').each(function() {
                    let name = $(this).attr('for');
                    $(this).attr('for', name.replace('[0]', '[' + TENANT_COUNT + ']'));
                });
                TENANT_COUNT++;
                return false;
            });

            $('#RemoveTenantButton').click(function(e) {
                e.preventDefault();
                $(this).parent().remove();
                return false;
            });
        });
    </script>

@endsection