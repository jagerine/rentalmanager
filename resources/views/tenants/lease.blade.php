@extends('layouts.app', ['heading' => 'Lease at '.$rentalUnit->name])

@section('content')

    <x-utilitymenu :items="[
        ['text' => 'Add Ledger Entry', 'url' => '/ledger/add?lease='.$lease->id, ['id' => 'AddLedgerButton'], 'icon' => 'plus-circle'], 
        ['text' => 'Update Lease Details', 'url' => route('tenants.update-lease-details', ['id' => $lease->id]), 'icon' => 'pencil-alt'] , 
        ['text' => 'Add Historical Note', 'url' => route('tenants.add-note', [$lease->id]), 'icon' => 'sticky-note'] , 
        ['text' => 'Review for past rents', 'url' => route('tenants.manual-charge-review-get', ['id' => $lease->id]), 'icon' => 'clock'] , 
    ]" />
 
    <h4 class="mx-5">
        <i class="fas fa-user-friends mr-2"></i>
        {{ $lease->display_name }}
    </h4>

@php
    $infoTabClass = $ledgerTabClass = $notesTabClass = 'bg-gray-700 text-gray-400 hover:text-gray-300';
    if($tab == 'info') {
        $infoTabClass = 'bg-gray-900 text-green-200 font-bold hover:bg-gray-900';
    } elseif($tab == 'ledger') {
        $ledgerTabClass = 'bg-gray-900 text-green-200 font-bold hover:bg-gray-900';
    } elseif($tab == 'notes') {
        $notesTabClass = 'bg-gray-900 text-green-200 font-bold hover:bg-gray-900';
    }


@endphp

<div class="tab-wrapper mt-6">
    <div class="tab-tabs">
        <ul class="m-0 ml-2">
            
            <li class="inline-block mr-1">
                <a class="{{ $ledgerTabClass }} rounded-t-lg hover:bg-gray-500 inline-block" href="/tenants/lease/{{ $lease->id }}">
                    <span class="px-4 py-2 inline-block text-sm md:text-md">
                        <i class="fas fa-dollar-sign mr-2 text-sm"></i>
                        Ledger
                    </span>
                </a>
            </li>
            <li class="inline-block mr-1">
                <a class="{{ $notesTabClass }} rounded-t-lg hover:bg-gray-500 inline-block" href="/tenants/lease/{{ $lease->id }}?tab=notes">
                    <span class="px-4 py-2 inline-block text-sm md:text-md">
                        <i class="fas fa-sticky-note mr-2 text-sm"></i>
                        Notes
                    </span>
                </a>
            </li>
            <li class="inline-block mr-1">
                <a class="{{ $infoTabClass }} rounded-t-lg hover:bg-gray-500 inline-block" href="/tenants/lease/{{ $lease->id }}?tab=info">
                    <span class="px-4 py-2 inline-block text-sm md:text-md">
                        <i class="fas fa-info-circle mr-2 text-sm"></i>
                        Lease Information
                    </span>
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content bg-gray-900 px-4 py-2 pb-10 rounded-lg">
        
        @includeWhen($tab == 'ledger', 'tenants.leaseledger')

        @includeWhen($tab == 'notes', 'tenants.leasenotes')

        @includeWhen($tab == 'info', 'tenants.leasedetails')

    </div>
</div>

<x-hr />


<div class="mb-16"></div>

@endsection
