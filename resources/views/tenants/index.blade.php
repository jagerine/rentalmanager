@extends('layouts.app', ['heading' => 'Tenants and Leases'])

@section('content')

    <x-utilitymenu :items="[
        ['text' => 'New Lease', 'url' => route('tenants.create'), 'icon' => 'plus-circle']
    ]" />

    <div class="mb-10">
        <x-datatable :data="$tableList" />
    </div>
    

@endsection