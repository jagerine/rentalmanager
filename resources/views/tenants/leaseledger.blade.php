

<div class="flex">

    <div class="w-full md:w-6/12">
        <dl>

            <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b border-gray-600 border-dotted">
                <dt class="text-sm font-medium text-gray-400">
                    Payment Status
                </dt>
                <dd class="mt-1 text-sm sm:mt-0 sm:col-span-2 mb-4 md:mb-0">
                    @if($lease['payment_status'] == 'Unpaid')
                        <span class="text-red-600 font-bold">{{ $lease['payment_status'] }}</span>
                    @elseif ($lease['payment_status'] == 'Credit')
                        <span class="text-green-600 font-bold">{{ $lease['payment_status'] }}</span>
                    @else
                        {{ $lease['payment_status'] }}
                    @endif
                </dd>

                @if($lease['payment_status'] != 'Current')

                <dt class="text-sm font-medium text-gray-400">
                    Balance
                </dt>
                <dd class="mt-1 text-sm sm:mt-0 sm:col-span-2 mb-4 md:mb-0">
                    @if($lease['payment_status'] == 'Unpaid')-@endif${{ abs($ledger['total']) }}
                </dd>

                @endif

            </div>
        </dl>

    </div>
    <div class="w-full md:w-6/12 text-right">
        <a href="/tenants/refresh-lease-data/{{ $lease['id'] }}" class="float-right cursor-pointer">
            <i class="fas fa-sync"></i>
        </a>
    </div>
    
</div>

<div class="flex my-5 text-sm">
    <div class="w-4/12 md:w-2/12">
        <span class="text-gray-400">Total Billed:</span><br> ${{ number_format($ledger['minus'], 2) }}
    </div>
    <div class="w-4/12 md:w-2/12">
        <span class="text-gray-400">Total Collected:</span><br> ${{ number_format($ledger['plus'], 2) }}
    </div>
    <div class="w-4/12 md:w-2/12">
        <span class="text-gray-400">Difference:</span> <br>
        @if($lease->payment_status == 'Unpaid')-@endif${{ number_format(abs($ledger['total']), 2) }}
    </div>
</div>

<table class="w-full lg:w-10/12 mt-8 text-xs">

    <thead class="bg-black">
        <tr>
            <th class="py-1 px-5 text-left"></th>
            <th class="py-1 px-5 text-left">Type</th>
            <th class="py-1 px-5 text-left">Date</th>
            <th class="py-1 px-5 text-left">Amount</th>
            <th class="py-1 px-5 text-left">Description</th>
            <th class="text-left pl-5">Flags</th>
            <th class="text-center">Comments</th>
        </tr>
    </thead>
    <tbody>

@foreach ($ledger['rows'] as $ledgerEntry)

        @php
            $entryFlag = ($ledgerEntry->needs_attention) ? ' ledger-flagged' : '';
            $entryOk = ($ledgerEntry->is_ok) ? ' ledger-okayed' : '';
            $historicalNotes = $ledgerEntry->historicalNotes();
            $hasNotesClass = ($historicalNotes->count() > 0) ? ' ledger-has-notes' : '';
        @endphp

        <tr class="">
            <td class="px-5 pt-3" width="1">
                <x-actionmenu :items="[
                    ['text' => 'Update Details', 'url' => '/ledger/edit/'.$ledgerEntry->id, 'icon' => 'pencil-alt'],
                    ['text' => 'Delete', 'url' => '/ledger/delete/'.$ledgerEntry->id, 'icon' => 'trash-alt'],
                ]" />
            </td>
            <td class="px-5 pt-3">
                <div>{{ $ledgerEntry->category }}</div>
                <div><small>{{ $ledgerEntry->type }}</small></div>
            </td>
            <td class="px-5 pt-3">{{ date('m/d/Y', strtotime($ledgerEntry->applied_date)) }}</td>
            <td class="px-5 pt-3 font-bold">
                @if($ledgerEntry->type == 'Debt')
                    <span class="">-${{ $ledgerEntry->amount }}<span>
                @else
                    <span class="text-green-500">+${{ $ledgerEntry->amount }}</span>
                @endif
            </td>
            <td class="px-5 pt-3">{{ $ledgerEntry->description }}</td>
            <td class="pl-4 whitespace-nowrap">
                <!-- ledger entry flags -->
                <i class="fas fa-flag cursor-pointer text-gray-400 ledger-flag mr-4 {{ $entryFlag }}" data-ledgerentry="{{ $ledgerEntry->id }}"></i>
                <i class="fas fa-check-circle cursor-pointer text-gray-400 ledger-ok {{ $entryOk }}" data-ledgerentry="{{ $ledgerEntry->id }}"></i>
                
            </td>
            <td class="pl-8 relative whitespace-nowrap">
                <!-- comment icon buttons -->
                <i class="fas fa-comment text-gray-400 cursor-pointer ml-2 mr-2 {{ $hasNotesClass }}" data-ledgerentry="{{ $ledgerEntry->id }}"></i>
                <i class="fas fa-plus text-gray-300 cursor-pointer ledger-comment" data-ledgerentry="{{ $ledgerEntry->id }}"></i>
                
                
                <!-- comment form container -->
                <div class="absolute right-0 top-0 mt-1 mr-2 w-64 ledger-comment-container z-10" style="display: none;" id="LedgerCommentContainer_{{ $ledgerEntry->id }}">
                    
                    <!-- closer button -->
                    <div class="text-right">
                        <i class="fas fa-times-circle text-gray-200 -ml-2 cursor-pointer ledger-comment-close" data-ledgerentry="{{ $ledgerEntry->id }}"></i>
                    </div>

                    <!-- visible box / container -->
                    <div class="bg-gray-700 rounded-lg shadow-lg p-2 -mt-1.5">                        
                        <div class="flex">
                            <div class="w-full">
                                <textarea class="border border-gray-900 w-full text-sm bg-gray-900 text-gray-200 py-0.5 px-1 rounded-md ledger-comment-input" 
                                    rows="2" placeholder="Comment" 
                                    data-ledgerentry="{{ $ledgerEntry->id }}">{{ $ledgerEntry->comment }}</textarea>
                            </div>
                        </div>
                        <div class="clear-both">
                            <button class="border border-gray-900 px-2 rounded-md bg-black ledger-comment-save" data-ledgerentry="{{ $ledgerEntry->id }}">
                                <i class="fas fa-save text-gray-400 cursor-pointer mr-1"></i>
                                Save
                            </button>
                        </div>
                    </div>

                </div>
                <!-- END comment form -->

            </td>
        </tr>
        <tr class="border-gray-700 border-b">
            <td width="1"></td>
            <td colspan="6" class="text-left text-gray-400 py-2 px-5" style="font-size: 0.9em;" id="LedgerEntry_HistoricalNotes">
                
                @foreach ($historicalNotes as $historicalNote)
                
                    @include('includes.ledgernote', ['historicalNote' => $historicalNote])

                @endforeach

            </td>
        </tr>

@endforeach

    </tbody>
</table>

<style>
    .ledger-okayed{
        color: #1dc9b7;
    }
    .ledger-flagged{
        color: #ffc107; 
    }
    .ledger-has-notes{
        color: #7dd6fa;
    }
</style>


<script>
    var CommentFormDisplayed = false;
    $(document).ready(function(){

        $(document.body).click(function(){
            if(CommentFormDisplayed){
                $('.ledger-comment-container:visible').hide();
                CommentFormDisplayed = false;
            }
        });

        $('i.ledger-comment-close').click(function(e){
            $(this).parent().parent().hide();
            CommentFormDisplayed = false;
            e.stopPropagation();
        });

        $('i.ledger-comment').click(function(e){
            CommentFormDisplayed = !CommentFormDisplayed;
            $(this)
                .parent()
                .find('.ledger-comment-container')
                .show()
                .find('textarea')
                .focus();
            e.stopPropagation();
        });

        $('.ledger-comment-container').click(function(e){
            e.stopPropagation();
        });

        $('.ledger-comment-save').click(function(e){
            e.stopPropagation();
            let ledgerEntryId = $(this).attr('data-ledgerentry');
            let url = '/notes/save-new/ledger';
            let comment = $(this).parents('.ledger-comment-container').find('textarea:first').val();
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    object_id: ledgerEntryId,
                    note: comment, 
                    _token: '{{ csrf_token() }}'
                },
                success: function(data){
                    if(data.success){
                        $('#LedgerCommentContainer_'+ledgerEntryId).hide();
                        $('#LedgerCommentContainer_'+ledgerEntryId).find('textarea:first').val('');
                        CommentFormDisplayed = false;
                    }
                }
            });
        });

        $('.ledger-ok').click(function(){
            let ledgerEntry = $(this).data('ledgerentry');
            if($(this).hasClass('ledger-okayed')){
                $(this).removeClass('ledger-okayed');
            }
            else {
                $(this).addClass('ledger-okayed');
            }
            $.ajax({
                url: '/ledger/toggle/ok/'+ledgerEntry, 
                success: function(data){
                    //Changed the class immediately assuming this would work out
                }
            });
        });

        $('.ledger-flag').click(function(){
            let ledgerEntry = $(this).data('ledgerentry');
            if($(this).hasClass('ledger-flagged')){
                $(this).removeClass('ledger-flagged');
            }
            else {
                $(this).addClass('ledger-flagged');
            }
            $.ajax({
                url: '/ledger/toggle/flag/'+ledgerEntry, 
                success: function(data){
                    //Changed the class immediately assuming this would work out
                }
            });
        });
    });
</script>