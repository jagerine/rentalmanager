

        <x-subheading text="Historical Notes" />

        @foreach ($leaseNotes as $historicalNote)

            <x-historicalnote :historicalNote="$historicalNote" />
        
        @endforeach