

        <x-subheading text="General Information" />
        <div class="flex gap-4 flex-col md:flex-row">
            <div class="w-full md:w-6/12">
                @php
                    $fields = [
                        'Rental Unit' => $rentalUnit->name,
                        'Tenant' => $lease->display_name,
                        'Dates' => date('m/d/Y', strtotime($lease->start_date)) . ' - ' . date('m/d/Y', strtotime($lease->end_date)) 
                    ];
                    if($lease->mtm_continuous){
                        $fields['Dates'] .= ' <br><span class="text-xs text-gray-300">(MTM thereafter)</span>';
                    }
                @endphp
                <dl>
                    @foreach ($fields as $key => $value)
                        
                    <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b border-gray-600 border-dotted">
                        <dt class="text-sm font-medium text-gray-400">
                            {{ $key }}
                        </dt>
                        <dd class="mt-1 text-sm sm:mt-0 sm:col-span-2 mb-4 md:mb-0">
                            {!! $value !!}
                        </dd>
                    </div>
            
                    @endforeach

                    <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b border-gray-600 border-dotted">
                        <dt class="text-sm font-medium text-gray-400">
                            Address
                        </dt>
                        <dd class="mt-1 text-sm sm:mt-0 sm:col-span-2 mb-4 md:mb-0">
                            <address class="text-xs">
                                {{ $rentalUnit->address }} {{ $rentalUnit->unit }} <br>
                                {{ $rentalUnit->city }}, {{ $rentalUnit->state }} {{ $rentalUnit->zip }} 
                                @if(!empty($rentalUnit->address))
                                    <x-maplink :search="urlencode($rentalUnit->address.' '.$rentalUnit->zip)" />
                                @endif
                            </address>
                        </dd>
                    </div>
                </dl>
            </div>
            <div class="w-full md:w-6/12">
                @php
                    $fields = [
                        'Payment Status' => $lease->payment_status,
                        'Rent' => $lease->rent.' / '.$lease->per,
                        'Due On' => $lease->due_on,
                        'Deposit' => $lease->deposit,
                        'Lease Status' => $lease->status
                    ];
                @endphp
                <dl>
                    @foreach ($fields as $key => $value)
                        
                    <div class="px-4 py-3 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 border-b border-gray-600 border-dotted">
                        <dt class="text-sm font-medium text-gray-400">
                            {{ $key }}
                        </dt>
                        <dd class="mt-1 text-sm sm:mt-0 sm:col-span-2 mb-4 md:mb-0">
                            {!! $value !!}
                        </dd>
                    </div>
            
                    @endforeach
                </dl>
            
            </div>
        </div>

        @if(!empty($tenants))
        
            <div class="h-1 border-b border-black border-dotted mx-5 my-10"></div>

            <x-subheading text="Tenant Details" />

            @php
                $tenantData = [
                    'headings' => ['Name', 'Email', 'Phone', 'Emergency Contact'],
                    'rows' => []
                ];
                foreach ($tenants as $tenant) 
                {
                    $stack = [
                        '_actions' => [
                            ['text' => 'Edit', 'url' => route('tenants.edit', ['id' => $tenant->id, 'lease_id' => $lease->id]), 'icon' => 'pencil-alt'],
                            ['text' => 'Delete', 'url' => route('tenants.delete', ['id' => $tenant->id]), 'icon' => 'trash-alt']
                        ], 
                        '_links' => [
                            'phone' => 'tel:'.$tenant->phone, 
                            'email' => 'mailto:'.$tenant->email,
                        ], 
                        'name' => $tenant->first_name.' '.$tenant->last_name,
                        'email' => $tenant->email,
                        'phone' => $tenant->phone,
                        'emergency_contact' => $tenant->emergency_contact 
                    ];
                    $tenantData['rows'][] = $stack;
                }  

            @endphp

            <x-datatable :data="$tenantData" />

        @endif