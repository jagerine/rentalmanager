@extends('layouts.app', ['heading' => 'New Ledger Entry'])

@section('content')

    @if(!$leaseId)
    
        {{ Form::open(['url' => route('tenants.add-ledger-entry-get'), 'method' => 'GET']) }}

            <div class="w-full md:w-6/12 lg:w-4/12">
                <x-forminput :params="['type' => 'select','name' => 'lease', 'options' => $leases]" /> 
                <x-submitbutton text="Go" />
            </div>

        {{ Form::close() }}

    @else
        
        <div>
            <span class="text-gray-300 inline-block w-48">Lease:</span>
            <strong><a href="/tenants/lease/{{ $lease->id }}" class="page-link">{{ $lease->display_name }}</a></strong><br>
            <span class="text-gray-300 inline-block w-48">Unit:</span>
            <strong>{{ $lease->rentalUnit()->name }}</strong><br>
            <span class="text-gray-300 inline-block w-48">Rent:</span>
            <strong>${{ $lease->rent }} / {{ $lease->per }}</strong><br>
            <span class="text-gray-300 inline-block w-48">Payment Status:</span>
            <strong>{{ $lease->payment_status }}</strong><br>
        </div>

        {{ Form::open(['url' => route('tenants.add-ledger-entry-post'), 'method' => 'POST']) }}
            
            <input type="hidden" name="lease" value="{{ $leaseId }}" />
            <input type="hidden" name="add_ledger_entry" value="1" />

            <div class="flex flex-col md:flex-row">
                <div class="w-full md:w-56">
                    <x-forminput :params="['name' => 'type', 'default' => 'month', 'type'=> 'select', 'label' => 'Type', 'options' => ['Credit' => 'Credit','Debt' => 'Debt']]" /> 
                </div>
            </div>
            <div class="flex flex-col md:flex-row">
                <div class="w-full md:w-48">
                    <x-forminput :params="['type' => 'date','name' => 'date', 'label' => 'Date']" />
                </div>
                <div class="w-full md:w-48">
                    <x-forminput :params="['type' => 'text','name' => 'amount', 'label' => 'Amount']" /> 
                </div>
                
            </div>
            <div class="flex flex-col md:flex-row">
                <div class="w-full md:w-5/12">
                    <x-forminput :params="['type' => 'text','name' => 'description', 'label' => 'Description']" />     
                </div>
            </div>

            <x-submitbutton />
                
        {{ Form::close() }}

    @endif 

</div>

@endsection