@extends('layouts.app', ['heading' => 'Edit Tenant Information'])

@section('content')

{{ Form::model($tenant, ['url' => ['tenants/edit/'. $tenant->id]]) }}
    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-56">
            <x-forminput :params="['name' => 'first_name', 'label' => 'First Name', 'type' => 'text']" />    
        </div>
        <div class="w-full md:w-56">
            <x-forminput :params="['name' => 'last_name', 'label' => 'Last Name', 'type' => 'text']" />
        </div>
    </div>
    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-64">
            <x-forminput :params="['name' => 'email', 'label' => 'Email', 'type' => 'email']" />
        </div>
        <div class="w-full md:w-64">
            <x-forminput :params="['name' => 'phone', 'label' => 'Phone', 'type' => 'phone']" />
        </div>
        <div class="w-full md:w-64">
            <x-forminput :params="['name' => 'alt_phone', 'label' => 'Alt Phone', 'type' => 'phone']" />
        </div>
    </div>
    <div class="flex flex-col md:flex-row">
        <div class="w-full md:w-96">
            <x-forminput :params="['name' => 'description', 'label' => 'Notes', 'type' => 'longtext']" />
        </div>
        <div class="w-full md:w-96">
            <x-forminput :params="['name' => 'emergency_contact', 'label' => 'Emergency Contact', 'type' => 'longtext']" />
        </div>
    </div>

    <div x-data="{ AdvancedOptions: false }" class="text-xs text-gray-400 cursor-pointer uppercase hover:text-gray-500">
        <i :class="AdvancedOptions ? 'fas fa-arrow-circle-down' : 'fas fa-arrow-circle-right'"></i>
        <span 
            @click="AdvancedOptions = !AdvancedOptions" 
            :aria-expanded="AdvancedOptions ? 'true' : 'false'" 
            :class="{ 'active': AdvancedOptions }"
            x-text="AdvancedOptions ? 'Hide Alt Address' : 'Use Alt Address'">
            
            Show Advanced</span>

        <div id="AdvancedOptions" x-show="AdvancedOptions" class="mt-4">

            <div class="flex flex-col md:flex-row">
                <div class="w-full md:w-72">
                    <x-forminput :params="['name' => 'address', 'label' => 'Address']" /> 
                </div>
                <div class="w-full md:w-32">
                    <x-forminput :params="['name' => 'unit', 'label' => 'Unit']" /> 
                </div>
            </div>
            
            <div class="flex flex-col md:flex-row">
                <div class="">
                    <x-forminput :params="['name' => 'city', 'label' => 'City']" /> 
                </div>
                <div class="w-full md:w-16">
                    <x-forminput :params="['name' => 'state', 'label' => 'ST', 'maxlength' => 2]" /> 
                </div>
                <div class="">
                    <x-forminput :params="['name' => 'zip', 'label' => 'Zip']" /> 
                </div>
            </div>

            
        </div>
    </div>

    <x-submitbutton />
    <input type="hidden" name="editTenant" value="1" />
    <input type="hidden" name="lease_id" value="{{ $leaseId ?? '0' }}" />

{{ Form::close() }}
<div class="mb-10"></div>

@endsection