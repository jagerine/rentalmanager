@extends('layouts.app', ['heading' => 'Past Rent Charge Review'])


@section('content') 


    @if(empty($date))

    <p class="w-full md:w-6/12">
        Use this tool to locate and review for past rent charges. You will be required to 
        confirm before any changes are made.
    </p>

    <p class="w-full md:w-6/12 my-6">
        <span class="inline-block w-24">Lease:</span> <strong>{{ $lease->display_name }}</strong> <br>
        <span class="inline-block w-24">Start Date:</span> <strong>{{ date('m/d/Y', strtotime($lease->start_date)) }} </strong>
    </p>

    <div class="w-full md:w-62 lg:w-64">
        {{ Form::open(['url' => '/tenants/manual-charge-review/'.$lease->id, 'method' => 'GET']) }}
        <x-forminput :params="['type' => 'date', 'name' => 'date']" />
        <x-submitbutton text="Review" />
        {{ Form::close() }}
    </div>

    @else

    <div class="w-full md:w-8/12">

        <p class="w-full md:w-6/12 my-6">
            <span class="inline-block w-24">Lease:</span> <strong>{{ $lease->display_name }}</strong> <br>
            <span class="inline-block w-24">Start Date:</span> <strong>{{ date('m/d/Y', strtotime($lease->start_date)) }} </strong>
        </p>

        <p class="mb-10">
            <strong>Past rent charge review for {{ date('m/d/Y', strtotime($date)) }}</strong>. The following charges are applicable to this lease:
        </p>

        <label><input type="checkbox" id="SelectAll" class="mr-5" /> Select All</label>
        <script>
            $(document).ready(function(){
                $('#SelectAll').click(function(){
                    if(this.checked){
                        $('input.possible-charge-select').each(function(){
                            this.checked = true;
                        });
                    }else{
                        $('input.possible-charge-select').each(function(){
                            this.checked = false;
                        });
                    }
                });
            });
        </script>

        {{ Form::open(['url' => '/tenants/manual-charge-review/'.$lease->id, 'method' => 'POST']) }}

        <table class="mt-5">
            <thead>
                <tr>
                    <th></th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Description</th>
                </tr>
                </tr>
            </thead>
            <tbody>

        @foreach ($possibleCharges as $possibleChargeDate)
            @php
                $possibleChargeDateTime = strtotime($possibleChargeDate);
            @endphp
                <tr class="border-t border-gray-600">
                    <td class="align-middle">
                        <input type="checkbox" class="mr-5 possible-charge-select" value="1" name="addRentCharge[{{ strtotime($possibleChargeDate) }}][select]">
                        <input type="hidden" value="{{ $possibleChargeDate }}" name="addRentCharge[{{ strtotime($possibleChargeDate) }}][date]">
                    </td>
                    <td class="align-middle">
                        <strong class="mr-5">{{ date('m/d/Y', strtotime($possibleChargeDate)) }}</strong>
                    </td>
                    <td>
                        <x-forminput :params="['type' => 'text','default' => $lease->rent, 'name' => 'addRentCharge['.strtotime($possibleChargeDate).'][total]']" />
                    </td>
                    <td>
                        <x-forminput :params="['type' => 'text',  'default' => 'Rent for '.date('F Y', $possibleChargeDateTime), 'name' => 'addRentCharge['.strtotime($possibleChargeDate).'][description]']" />
                    </td>
                </tr>

        @endforeach

            </tbody>
        </table>

        <x-submitbutton text="Save Selected" />
        <input type="hidden" name="save_charges" value="1" />
        <input type="hidden" name="date" value="{{ $date }}" />
        <input type="hidden" name="lease_id" value="{{ $lease->id }}" />

        {{ Form::close() }}

    </div>

    @endif

@endsection