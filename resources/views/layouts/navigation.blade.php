<ul class="list-reset lg:flex flex-1 items-center px-4 md:px-0">
    <li class="mr-6 my-2 md:my-0">
        <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
            <i class="fas fa-tachometer-alt fa-fw mr-3"></i>
            <span class="pb-1 md:pb-0 text-sm">
                {{ __('Dashboard') }}
            </span>
        </x-nav-link>
    </li>
    
    <li class="mr-6 my-2 md:my-0">
        <x-nav-link :href="route('units')" :active="request()->routeIs('units')">
            <i class="fas fa-home fa-fw mr-3"></i>
            <span class="pb-1 md:pb-0 text-sm">Units</span>
        </x-nav-link>
    </li>
    <li class="mr-6 my-2 md:my-0">
        <x-nav-link href="/reports">
            <i class="fas fa-chart-bar fa-fw mr-3"></i>
            <span class="pb-1 md:pb-0 text-sm">Reports</span>
        </x-nav-link>
    </li>
    <li class="mr-6 my-2 md:my-0">
        <x-nav-link href="/plugins">
            <i class="fas fa-plug fa-fw mr-3"></i>
            <span class="pb-1 md:pb-0 text-sm">Plugins</span>
        </x-nav-link>
    </li>
    <li class="mr-6 my-2 md:my-0">
        <a href="#" id="OpenNewObjectModal" class="block py-1 md:py-3 align-middle text-gray-200 no-underline hover:text-gray-100 border-b-2 border-gray-900  hover:border-pink-400">
            <i class="fas fa-plus-circle fa-fw mr-3"></i>
            <span class="pb-1 md:pb-0 text-sm">Add</span>
        </a>
    </li>
    
</ul>


