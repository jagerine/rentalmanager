<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Rental Manager Pro') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        
    </head>
    <body class="bg-black-alt font-sans leading-normal tracking-normal min-h-screen flex flex-col bg-gray-800">

        <nav id="header" class="bg-gray-900 fixed w-full z-10 top-0 shadow px-2">
                <div class="w-full container mx-auto flex flex-wrap items-center mt-0 pt-3 pb-3 md:pb-0">
                        
                    <div class="w-1/2 pl-2 md:pl-0">
                        <a class="text-gray-400 text-base xl:text-xl no-underline hover:no-underline font-bold"  href="/"> 
                            <i class="fas fa-door-open text-gray-400 pr-1"></i> Rental Manager Pro
                        </a>
                    </div>
                    <div class="w-1/2 pr-0">
                        <div class="flex relative inline-block float-right">
                        
                          <div class="relative text-sm text-gray-100">
                              <button id="userButton" class="flex items-center focus:outline-none mr-3">
                                <span class="hidden md:inline-block text-gray-100">
                                    {{ Auth::user()->name }}
                                    
                                </span>
                                <svg class="pl-2 h-2 fill-current text-gray-100" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/></g></svg>
                              </button>
                              <div id="userMenu" class="bg-gray-900 rounded shadow-md mt-2 absolute mt-12 top-0 right-0 min-w-full overflow-auto z-30 invisible">
                                  <ul class="list-reset">
                                    <li><a href="#" class="px-4 py-2 block text-gray-100 hover:bg-gray-800 no-underline hover:no-underline">My account</a></li>
                                    <li><a href="#" class="px-4 py-2 block text-gray-100 hover:bg-gray-800 no-underline hover:no-underline">Notifications</a></li>
                                    <li><hr class="border-t mx-2 border-gray-400"></li>
                                    <li>
                                        <form method="POST" action="{{ route('logout') }}">
                                            @csrf
                                            
                                            <a href="#" 
                                                onclick="event.preventDefault();
                                                    this.closest('form').submit();" 
                                                    class="px-4 py-2 block text-gray-100 hover:bg-gray-800 no-underline hover:no-underline">Logout</a>
                                        </form>
                                  </ul>
                              </div>
                          </div>
        
        
                            <div class="block lg:hidden pr-4">
                            <button id="nav-toggle" class="flex items-center px-3 py-2 border rounded text-gray-500 border-gray-600 hover:text-gray-100 hover:border-teal-500 appearance-none focus:outline-none">
                                <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
                            </button>
                        </div>
                        </div>
        
                    </div>
        
                    
                    <div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block mt-2 lg:mt-0 bg-gray-900 z-20" id="nav-content">
                        
                        @include('layouts.navigation')
                        
                        <div class="relative pull-right pl-4 pr-4 md:pr-0">
                            <input type="search" placeholder="Search" class="w-full bg-gray-900 text-sm text-gray-400 transition border border-gray-600 focus:outline-none focus:border-gray-200 rounded py-1 px-2 pl-10 appearance-none leading-normal">
                            <div class="absolute search-icon mt-0.5" style="top: 0.375rem;left: 1.75rem;">
                                <svg class="fill-current pointer-events-none text-gray-500 w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"></path>
                                </svg>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </nav>
        
            <!--Container-->
            <div class="container w-full mx-auto pt-28 flex-grow px-2 md:px-2">
                @php
                  if(!isset($heading)){
                    $heading = '';
                  } 
                @endphp
                <h2 class=" text-2xl border-b border-gray-600 mb-2" style="color: powderblue;">{{ $heading }}</h2>

                @if ($errors->all())
                <div class="bg-yellow-50 border-l-4 border-b-2 border-yellow-500 text-yellow-700 px-4 py-2 rounded-md mb-8 mx-2" role="alert">
                     
                    <p>
                      <i class="fas fa-exclamation-triangle float-left mr-2 mb-2 mt-1 align-middle"></i>
                      <p class="inline-block">
                        @foreach ($errors->all() as $errors)
                            {{ $errors }}<br>
                        @endforeach
                      </p> 
                      <div class="clear-both"></div>
                    </p>
                      
                  </div>

                  @endif

                  @if(Session::has('message'))

                  <div x-data="{ show: true }" 
                    x-show="show" x-init="setTimeout(() => show = false, 3000)" 
                    class="bg-green-100 border-l-4 border-green-500 text-green-700 px-4 py-2 rounded-md mb-8" 
                    role="alert">

                      <p>
                          <i class="fas fa-check"></i>
                          {{ Session::get('message') }}
                      </p>
                  </div>
                @endif
                
                <div id="ContentContainer" class="text-gray-100">
                  @yield('content')

                </div> <!--End #ContentContainer-->
                

            </div> 
            <!--/container-->
            
            <footer class="bg-gray-900 border-t border-gray-400 shadow bottom-0 w-full">	
                <div class="container max-w-md mx-auto py-8 text-gray-600 text-center">
        
                    Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved.
        
                
                </div>
            </footer>
        
        <script>
            
            
            /*Toggle dropdown list*/
            /*https://gist.github.com/slavapas/593e8e50cf4cc16ac972afcbad4f70c8*/
        
            var userMenuDiv = document.getElementById("userMenu");
            var userMenu = document.getElementById("userButton");
            
            var navMenuDiv = document.getElementById("nav-content");
            var navMenu = document.getElementById("nav-toggle");
            
            document.onclick = check;
        
            function check(e){
              var target = (e && e.target) || (event && event.srcElement);
        
              //User Menu
              if (!checkParent(target, userMenuDiv)) {
                // click NOT on the menu
                if (checkParent(target, userMenu)) {
                  // click on the link
                  if (userMenuDiv.classList.contains("invisible")) {
                    userMenuDiv.classList.remove("invisible");
                  } else {userMenuDiv.classList.add("invisible");}
                } else {
                  // click both outside link and outside menu, hide menu
                  userMenuDiv.classList.add("invisible");
                }
              }
              
              //Nav Menu
              if (!checkParent(target, navMenuDiv)) {
                // click NOT on the menu
                if (checkParent(target, navMenu)) {
                  // click on the link
                  if (navMenuDiv.classList.contains("hidden")) {
                    navMenuDiv.classList.remove("hidden");
                  } else {navMenuDiv.classList.add("hidden");}
                } else {
                  // click both outside link and outside menu, hide menu
                  navMenuDiv.classList.add("hidden");
                }
              }
              
            }
        
            function checkParent(t, elm) {
              while(t.parentNode) {
                if( t == elm ) {return true;}
                t = t.parentNode;
              }
              return false;
            }
        
        
        </script>
        

        <!--Modal-->
  <div class="modal opacity-0 pointer-events-none fixed w-full h-full top-0 left-0 flex items-center justify-center" id="NewObjectModal">
    <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>
    <div class="modal-container bg-gray-600 w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
      
      <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
        <i class="fas fa-times text-white text-2xl"></i>
      </div>

      <!-- Add margin if you want to see some of the overlay behind the modal-->
      <div class="modal-content py-4 text-left px-6">
        <!--Title-->
        <div class="flex justify-between items-center pb-3 text-gray-50">
          <p class="text-2xl font-bold">Add New:</p>
          <div class="modal-close cursor-pointer z-50">
            <i class="fas fa-times text-gray-50 text-2xl"></i>
          </div>
        </div>
        
        <ul class="">
          <li>
            <a href="/tenants/add-ledger-entry" class="block no-underline bg-gray-200 rounded-md mb-2 cursor-pointer hover:bg-gray-400 transition ease-in-out hover:text-white duration-200">
              <i class="fas fa-money-bill-wave text-white bg-gray-400 rounded-l-md p-3 w-10 text-center "></i>
              <span>Ledger Entry</span>
            </a>
          </li>
          <li>
              <a href="/tenants/create" class="block no-underline bg-gray-200 rounded-md mb-2 cursor-pointer hover:bg-gray-400 transition ease-in-out hover:text-white duration-200">
                <i class="fas fa-address-card text-white bg-gray-400 rounded-l-md p-3 w-10 text-center "></i>
                <span>Tenant / Lease</span>
              </a>
          </li>
          <li>
              <a href="/units/create" class="block no-underline bg-gray-200 rounded-md mb-2 cursor-pointer hover:bg-gray-400 transition ease-in-out hover:text-white duration-200">
                <i class="fas fa-home text-white bg-gray-400 rounded-l-md p-3 w-10 text-center "></i>
                <span>Rental Unit</span>
              </a>
          </li>
          

        </ul>

      </div>
    </div>
  </div>

  <script>
    var openmodal = document.querySelectorAll('#OpenNewObjectModal');
    for (var i = 0; i < openmodal.length; i++) {
      openmodal[i].addEventListener('click', function(event){
    	event.preventDefault()
    	toggleModal()
      })
    }
    
    const overlay = document.querySelector('.modal-overlay')
    overlay.addEventListener('click', toggleModal)
    
    var closemodal = document.querySelectorAll('.modal-close')
    for (var i = 0; i < closemodal.length; i++) {
      closemodal[i].addEventListener('click', toggleModal)
    }
    
    document.onkeydown = function(evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
    	isEscape = (evt.key === "Escape" || evt.key === "Esc")
      } else {
    	isEscape = (evt.keyCode === 27)
      }
      if (isEscape && document.body.classList.contains('modal-active')) {
    	toggleModal()
      }
    };
    
    
    function toggleModal () {
      const body = document.querySelector('body')
      const modal = document.querySelector('#NewObjectModal')
      modal.classList.toggle('opacity-0')
      modal.classList.toggle('pointer-events-none')
      body.classList.toggle('modal-active')
    }

    $(document).ready(function(){
      $('input[data-type=date]').attr('type', 'date')
    });
    
     
  </script>

  

    </body>
</html>
