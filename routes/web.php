<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/home', 'App\Http\Controllers\DashboardController@index')->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/', 'App\Http\Controllers\DashboardController@index')->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/test', 'App\Http\Controllers\DashboardController@test')->name('test');

## Tenants
Route::get('/tenants', 'App\Http\Controllers\TenantsController@index')->middleware(['auth', 'verified'])->name('tenants');
Route::get('/tenants/create', 'App\Http\Controllers\TenantsController@create')->middleware(['auth', 'verified'])->name('tenants.create');
Route::get('/tenants/view/{id}', 'App\Http\Controllers\TenantsController@show')->middleware(['auth', 'verified'])->name('tenants.show');
Route::get('/tenants/edit/{id}', 'App\Http\Controllers\TenantsController@edit')->middleware(['auth', 'verified'])->name('tenants.edit');
Route::post('/tenants/edit/{id}', 'App\Http\Controllers\TenantsController@edit')->middleware(['auth', 'verified'])->name('tenants.edit');
Route::get('/tenants/delete/{id}', 'App\Http\Controllers\TenantsController@delete')->middleware(['auth', 'verified'])->name('tenants.delete');
Route::post('/tenants/store', 'App\Http\Controllers\TenantsController@store')->middleware(['auth', 'verified'])->name('tenants.store');
Route::post('/tenants/update/{id}', 'App\Http\Controllers\TenantsController@update')->middleware(['auth', 'verified'])->name('tenants.update');
Route::get('/tenants/lease-term/{id}', 'App\Http\Controllers\TenantsController@leaseTerm')->middleware(['auth', 'verified'])->name('tenants.lease-term');
Route::get('/tenants/update-lease-details/{id}', 'App\Http\Controllers\TenantsController@updateLeaseDetails')->middleware(['auth', 'verified'])->name('tenants.update-lease-details');
Route::get('/tenants/refresh-lease-data/{id}', 'App\Http\Controllers\TenantsController@refreshLeaseData')->middleware(['auth', 'verified'])->name('tenants.refresh-lease-data');
Route::get('/tenants/lease/{id}', 'App\Http\Controllers\TenantsController@lease')->middleware(['auth', 'verified'])->name('tenants.lease');
Route::get('/tenants/manual-charge-review/{id}', 'App\Http\Controllers\TenantsController@manualChargeReview')->middleware(['auth', 'verified'])->name('tenants.manual-charge-review-get');
Route::post('/tenants/manual-charge-review/{id}', 'App\Http\Controllers\TenantsController@manualChargeReview')->middleware(['auth', 'verified'])->name('tenants.manual-charge-review-post');
//Route::get('/tenants/add-ledger-entry', 'App\Http\Controllers\TenantsController@addLedgerEntry')->middleware(['auth', 'verified'])->name('tenants.add-ledger-entry-get');
//Route::post('/tenants/add-ledger-entry', 'App\Http\Controllers\TenantsController@addLedgerEntry')->middleware(['auth', 'verified'])->name('tenants.add-ledger-entry-post');
Route::get('/tenants/add-note/{leaseId}', 'App\Http\Controllers\TenantsController@addNote')->middleware(['auth', 'verified'])->name('tenants.add-note');
Route::post('/tenants/store-note/{type}', 'App\Http\Controllers\TenantsController@storeNote')->middleware(['auth', 'verified'])->name('tenants.store-note');

## Ledger
Route::get('/ledger', 'App\Http\Controllers\LedgerController@index')->middleware(['auth', 'verified'])->name('ledger');
Route::get('/ledger/toggle/{type}/{id}', 'App\Http\Controllers\LedgerController@toggle')->middleware(['auth', 'verified'])->name('ledger.toggle');
Route::get('/ledger/delete/{id}', 'App\Http\Controllers\LedgerController@delete')->middleware(['auth', 'verified'])->name('ledger.delete');
Route::get('/ledger/add', 'App\Http\Controllers\LedgerController@add')->middleware(['auth', 'verified'])->name('ledger.add');
Route::post('/ledger/add', 'App\Http\Controllers\LedgerController@add')->middleware(['auth', 'verified'])->name('ledger.add');
Route::get('/ledger/edit/{id}', 'App\Http\Controllers\LedgerController@editLedgerEntry')->middleware(['auth', 'verified'])->name('ledger.edit');
Route::post('/ledger/edit/{id}', 'App\Http\Controllers\LedgerController@editLedgerEntry')->middleware(['auth', 'verified'])->name('ledger.edit');

//Notes
Route::get('/notes/add/{objectType}/{objectId}', 'App\Http\Controllers\NotesController@add')->middleware(['auth', 'verified'])->name('notes.add');
Route::post('/notes/save-new/{objectType}', 'App\Http\Controllers\NotesController@saveNew')->middleware(['auth', 'verified'])->name('notes.save-new');

## Units
Route::get('/units', 'App\Http\Controllers\UnitsController@index')->middleware(['auth', 'verified'])->name('units');
Route::get('/units/create', 'App\Http\Controllers\UnitsController@create')->middleware(['auth', 'verified'])->name('units.create');
Route::get('/units/show/{id}', 'App\Http\Controllers\UnitsController@show')->middleware(['auth', 'verified'])->name('units.show');
Route::get('/units/edit/{id}', 'App\Http\Controllers\UnitsController@edit')->middleware(['auth', 'verified'])->name('units.edit');
Route::post('/units/store', 'App\Http\Controllers\UnitsController@store')->middleware(['auth', 'verified'])->name('units.store');
Route::post('/units/update/{id}', 'App\Http\Controllers\UnitsController@update')->middleware(['auth', 'verified'])->name('units.update');
Route::get('/units/delete/{id}', 'App\Http\Controllers\UnitsController@delete')->middleware(['auth', 'verified'])->name('units.delete');
Route::get('/units/vacate/{id}', 'App\Http\Controllers\UnitsController@vacate')->middleware(['auth', 'verified'])->name('units.vacate');
Route::post('/units/vacate/{id}', 'App\Http\Controllers\UnitsController@vacate')->middleware(['auth', 'verified'])->name('units.vacate');

require __DIR__.'/auth.php';
