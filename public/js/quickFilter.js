/**
 * QuickSearch Jquery Plugin
 * Provides an instant onpage search and filter of
 * text that has already been delivered to the page.
 * This is useful if you have long lists from your database
 * that could use a quick instant filter down.
 *
 * Usage:
 * $('#ProgramSearchInput').quickFilter();
 *
 * You can optionally send any combination of the
 * following settings:
 * 
 * $('#ProgramSearchInput').quickFilter({
 *  searchableClass : 'quickSearchable', 
 *  searchArea : '#quickSearchArea',
 *  hideable : 'tr', 
 * });
 *
 * searchableClass is the class name of element that has the text
 *   that should be searched
 *   default: quickSearchable
 *
 *  searchArea is the CSS selector for the element that contains the
 *    elements that should be searched. This limits the scope of DOM
 *    searches
 *    default: body
 *
 *  hideable is the CSS selector for the elements that should
 *    be hidden when a match is not found. 
 *    default: tr
 */
(function($){
  $.fn.quickFilter = function(){
      if (arguments[0])
        var settings = arguments[0];
      else
        var settings = {};  
      
      //apply default settings
      var searchableClass = '.quickSearchable';
      var searchArea = '#QuickSearchArea';
      var hideableParent = 'tr';
      
      //settings override
      if (settings.searchableClass != undefined)
          searchableClass = '.'+settings.searchableClass;
      if (settings.hideable != undefined)
          hideableParent = settings.hideable;
      if (settings.searchArea != undefined)
          searchArea = settings.searchArea;
          
      //Apply typing event callback
      //	if value is blank, display all. Otherwise, find
      //	searchable texts and show parent
      $(this).keyup(function(){
          var val = $.trim(this.value.toLowerCase());
          if (val == '')
          {
              if(hideableParent == 'tr')
                $(searchArea).find(hideableParent).css('display', 'table-row');   
              else
                $(searchArea).find(hideableParent).show(); 
          }
          else
          {
              $area = $(searchArea);
              $area.find(hideableParent).each(function(){
                  var hasMatch = false;
                  var $searchables = $(this).find(searchableClass);
                  if ($searchables.length > 0)
                  {
                      $searchables.each(function(){
                          var t = $(this).text().toLowerCase();
                          if ( t.indexOf(val) > -1 )
                              hasMatch = true;
                      });
                      if (hasMatch)
                      {
                        if(hideableParent == 'tr')
                          $(this).css('display', 'table-row');
                        else
                          $(this).show(); 
                      }
                      else
                      {
                        if(hideableParent == 'tr')
                          $(this).css('display', 'none');
                        else
                          $(this).hide(); 
                      }
                  }
                  
              });
              if ($area.length < 1)
              {
                $.error('No searchable area found. ('+searchArea+')'); 
              }
          }
      });
  };
}( jQuery ));