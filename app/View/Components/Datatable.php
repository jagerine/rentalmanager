<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Datatable extends Component
{

    public $data; 
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($data, $textFilter = 0)
    {
        $this->data = $data;
        $this->textFilter = $textFilter;
    }
    
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.datatable', ['data' => $this->data, 'textFilter' => $this->textFilter]);
    }
}
