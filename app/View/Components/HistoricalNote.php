<?php

namespace App\View\Components;

use Illuminate\View\Component;

class HistoricalNote extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($historicalNote)
    {
        $this->historicalNote = $historicalNote;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.historical-note', [
            'historicalNote' => $this->historicalNote
        ]);
    }
}
