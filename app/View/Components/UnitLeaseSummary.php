<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\RentalUnit;

class UnitLeaseSummary extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    private function _formatPaymentStatusDescription($json)
    {
        if (empty($json)) {
            return '';
        }

        $output = [];
        $statusAmounts = json_decode($json, true);
        if (!$statusAmounts) {
            return 'recalc';
        }

        foreach ($statusAmounts as $statusCategory => $statusAmount) {
            $output[] = $statusCategory . ': ' . $statusAmount;
        }
        return ' <small class="payment-status-description text-gray-100 block">'.join('<br>', $output).'</small>';
    }

    public function getTableList()
    {
        $rentalUnitSummary = RentalUnit::getSummary();
        $headings = ['Unit', 'Tenant', 'Rent', 'Status'];
        $rows = [];
        foreach ($rentalUnitSummary as $key => $value) {
            $stack = [
                '_actions' => [
                    ['text' => 'Edit Unit Info', 'url' => '/units/edit/'.$value['id'], 'icon' => 'home'],
                    ['text' => 'Remove Rental Unit', 'url' => '/units/delete/'.$value['id'], 'icon' => 'fire']
                ],
                '_links' => [
                    'name' => '/units/show/'.$value['id']
                ],
                'tenant' => $value->display_name ?? '(Vacant)',
                'name' => $value->name,
                'rent' => ($value->current_lease_id) ? $value->rent .' / '. $value->per : '',
                'status' => $value->payment_status
            ];
            if ($value->payment_status == 'Unpaid' && !empty($value->payment_status_description)) {
                $stack['status'] .= $this->_formatPaymentStatusDescription($value->payment_status_description);
            }
            if ($value->status == 'Occupied') {
                $stack['_actions'][] = ['divider' => 1];
                $stack['_actions'][] = ['text' => 'Edit Lease Info', 'url' => '/lease/update/'.$value['current_lease_id'], 'icon' => 'signature'];
                $stack['_actions'][] = ['text' => 'Vacate', 'url' => '/units/vacate/'.$value['id'], 'icon' => 'handshake'];
                $stack['_actions'][] = ['text' => 'Refresh Lease Data', 'url' => '/tenants/refresh-lease-data/'.$value['current_lease_id'], 'icon' => 'sync'];
                $stack['_links']['tenant'] = '/tenants/lease/'.$value['current_lease_id'];
            } else {
                array_unshift($stack['_actions'], ['text' => 'Add Lease', 'url' => '/tenants/create?unit_id='.$value['id'], 'icon' => 'signature']);
            }
            $rows[] = $stack;
        }
        return [
            'headings' => $headings,
            'rows' => $rows,
            'options' => [
                'status' => [
                    'escape' => false
                ],
            ]
        ];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.units.lease-summary', $this->getTableList());
    }
}
