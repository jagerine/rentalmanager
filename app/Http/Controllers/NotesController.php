<?php

namespace App\Http\Controllers;

use App\Models\HistoricalNote;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    //
    public function saveNew($objectType){
        $requestAll = request()->all();
        $objectId = $requestAll['objectId'] ?? $requestAll['id'] ?? 0;
        if(empty($objectId)){
            return response()->json(['error' => 'Object id is required.', 'success' => false]);
        }
        $note = new HistoricalNote();
        $note->note = $requestAll['note'];
        $note->object_id = $objectId;
        $note->object_type = $objectType;
        $note->save();

        if(request()->ajax()){
            return response()->json(['success' => true, 'note' => $note]);
        }
        $redirectTo = session('add-note-redirect') ?? request()->headers()->referer;
        return redirect($redirectTo)->with('message', 'Note saved successfully');
    }

    public function add($objectType, $objectId){
        return view('notes.add', [
            'referer' => request()->headers->get('referer'),
            'objectType' => $objectType,
            'objectId' => $objectId,
        ]);
    }

}
