<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RentalUnit;
use App\Models\Lease;

class UnitsController extends Controller
{
    public function index()
    {
        $summary = RentalUnit::getSummary();

        $statusCounts = RentalUnit::getStatusCounts();
        return view('units.index', [
            'heading' => 'New Rental Unit',
            //'tableData' => RentalUnit::getTableList(),
            'statusCounts' => $statusCounts,
            'summary' => $summary,
        ]);
    }

    public function create()
    {
        return view('units.createOrUpdate', [
            'heading' => 'New Rental Unit',
        ]);
    }
    public function edit($id)
    {
        $rentalUnit = RentalUnit::find($id);
        return view('units.createOrUpdate', [
            'heading' => 'Update Rental Unit',
            'rentalUnit' => $rentalUnit,
        ]);
    }

    /**
     * Update rental unit record in database
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $validated = $this->validate($request, RentalUnit::getPostValidation());
        $rentalUnit = RentalUnit::find($id);
        $rentalUnit->update($validated);
        return redirect('/units')->with('message', 'Rental unit updated successfully');
    }

    /**
     * Save new rental unit to database
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, RentalUnit::getPostValidation());
        $validated['status'] = 'Vacant';
        RentalUnit::create($validated);
        return redirect('/units')->with('message', 'Rental unit added successfully');
    }

    
    public function delete($id)
    {
        $unit = RentalUnit::find($id);
        $unit->delete();
        return redirect('/units')->with('message', 'Rental unit deleted successfully');
    }

    public function show($id)
    {
        $rentalUnit = RentalUnit::find($id);
        $menuOptions = [];
        if ($rentalUnit->status == 'Vacant') {
            $menuOptions[] = ['text' => 'New Lease', 'url' => route('units.create'), 'icon' => 'plus-circle'];
        }
        return view('units.show', [
            'heading' => 'Rental Unit',
            'rentalUnit' => $rentalUnit,
            'menuOptions' => $menuOptions,
        ]);
    }

    public function vacate(int $rentalUnitID, Request $request)
    {
        //dd($request);
        $rentalUnit = RentalUnit::find($rentalUnitID);

        if ($request->method() == 'POST') {
            if (empty($rentalUnit->current_lease_id)) {
                redirect(request()->headers->get('referer'))
                    ->with('message', 'No current lease was found.');
            }

            $lease = Lease::find($rentalUnit->current_lease_id);
            $lease->status = 'Inactive';
            $lease->term_date = date('Y-m-d', strtotime($request->post('term_date')));
            $lease->save();

            $rentalUnit->setStatus();

            return redirect('/units')->with('message', 'Rental unit vacated successfully.');
        }


        return view('units.vacate', [
            'heading' => 'Vacate Rental Unit',
            'rentalUnit' => RentalUnit::find($rentalUnitID)
        ]);
    }
}
