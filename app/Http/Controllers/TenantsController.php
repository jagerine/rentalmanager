<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RentalUnit;
use App\Models\Lease;
use App\Models\LeaseTenant;
use App\Models\Tenant;

class TenantsController extends Controller
{
    public function __invoke()
    {
    }

    public function index()
    {
        $tableList = Lease::getTableList();
        return view('tenants.index', [
            'tableList' => $tableList,
        ]);
    }

    public function refreshLeaseData($leaseId)
    {
        Lease::find($leaseId)
            ->setDisplayName()
            ->setPaymentStatus()
            ->setNextDueDate()
            ->rentalUnit()
            ->setStatus();
        return redirect()->back()->with('message', 'Lease data has been refreshed.');
    }

    public function create()
    {
        $rentalUnits = RentalUnit::pluck('name', 'id');
        $unitId = request()->all()['unit_id'] ?? null;
        return view('tenants.createOrUpdate', [
            'rentalUnits' => $rentalUnits,
            'unitId' => $unitId,
        ]);
    }

    public function edit($tenantId)
    {
        $tenant = Tenant::find($tenantId);
        $post = request()->all();
        if (!empty($post['editTenant'])) {
            foreach ($post as $field => $value) {
                if (in_array($field, $tenant->fillable)) {
                    $tenant->{$field} = $value;
                }
            }
            $tenant->save();

            if(!empty($post['lease_id'])){
                Lease::find($post['lease_id'])->setDisplayName();
                $redirect = '/tenants/lease/'.$post['lease_id'].'?tab=info';
            } else {
                $redirect = request()->headers->get('referer');
            }

            return redirect($redirect)->with('message', 'Tenant updated successfully');
        }

        return view('tenants.editTenant', [
            'tenant' => $tenant,
            'referer' => request()->headers->get('referer') , 
            'leaseId' => $post['lease_id'] ?? null,
        ]);
    }

    public function store(Request $request)
    {
        $post = $request->all();

        $tenants = $leaseDisplayName = [];
        foreach ($post['Tenant'] as $tenant) {
            $tenants[] = Tenant::create($tenant);
            $leaseDisplayName[] = $tenant['first_name'] . ' ' . $tenant['last_name'];
        }

        $lease = Lease::create([
            'rental_unit_id' => $post['Lease']['rental_unit_id'],
            'display_name' => implode(', ', $leaseDisplayName),
            'start_date' => $post['Lease']['start_date'],
            'end_date' => $post['Lease']['end_date'],
            'mtm_continuous' => $post['Lease']['mtm_continuous'],
            'deposit' => $post['Lease']['deposit'],
            'rent' => $post['Lease']['rent'],
            'per' => $post['Lease']['per'],
            'due_on' => $post['Lease']['due_on'],
            'status' => 'Active'
        ]);

        foreach ($tenants as $tenant) {
            LeaseTenant::create([
                'lease_id' => $lease->id,
                'tenant_id' => $tenant->id
            ]);
        }

        $lease->setPaymentStatus();
        $lease->rentalUnit()->setStatus();
        return redirect('/tenants')->with('message', 'Lease added successfully.');
    }

    public function lease($leaseId)
    {
        $lease = Lease::find($leaseId);
        
        //Determine set tab
        $query = request()->all();
        $tab = 'ledger';
        if (isset($query['tab']) && in_array($query['tab'], ['info', 'ledger', 'notes'])) {
            $tab = $query['tab'];
        }

        //retrieve ledger data if needed
        if ($tab == 'info') {
            $leaseLedger = null;
        } else {
            $leaseLedger = $lease->getLedger();
        }

        $leaseNotes = [];
        if ($tab == 'notes') {
            $leaseNotes = $lease->getNotes();
        }
        
        return view('tenants.lease', [
            'lease' => $lease,
            'ledger' => $leaseLedger,
            'rentalUnit' => $lease->rentalUnit(),
            'tenants' => $lease->tenants(),
            'tab' => $tab,
            'leaseNotes' => $leaseNotes,
        ]);
    }

    public function addNote($leaseId)
    {
        session(['add-note-redirect' => "/tenants/lease/$leaseId?tab=notes",]);
        $lease = Lease::find($leaseId);
        return view('notes.add', [
            
            'objectType' => 'lease',
            'objectId' => $lease->id, 
            'title' => 'Add Note: '.$lease->display_name,
        ]);
    }

    public function storeNote($type)
    {
        $objectId = request()->all()['id'];
        dd($objectId); 
    }

    public function manualChargeReview($leaseId)
    {
        $date = $possibleCharges = null;
        $lease = Lease::find($leaseId);
        $query = request()->all();
        
        //Handle posted results
        if (!empty($query['save_charges'])) {
            $message = 'no charge was added.';
            if (!empty($query['addRentCharge'])) {
                foreach ($query['addRentCharge'] as $addRentCharge) {
                    if (empty($addRentCharge['select'])) {
                        continue;
                    }
                    $lease->addLedgerEntry(
                        'Debt',
                        'Rent', 
                        $addRentCharge['total'],
                        $addRentCharge['date'],
                        $addRentCharge['description'],
                        [ 'is_regular_rent' => 1  ]
                    );
                    $message = 'a new charge has been added.';
                }
            }
            $lease->setPaymentStatus()->setStatus();
            return redirect('/tenants/lease/'.$lease->id)->with('message', 'This lease has been reviewed and '.$message);
        }


        if (isset($query['date'])) {
            $date = $query['date'];
        }
        if ($date) {
            $possibleCharges = $lease->getPossibleCharges(strtotime($date));
        }
        return view('tenants.manualChargeReview', compact('possibleCharges', 'lease', 'leaseId', 'date'));
    }
}
