<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lease;
use App\Models\LedgerEntry;

class LedgerController extends Controller
{
    
    public function update($ledgerEntryId){
        $ledgerEntry = LedgerEntry::find($ledgerEntryId);
        return view('ledger.update', [
            'ledgerEntry' => $ledgerEntry,
            'ledgerEntryId' => $ledgerEntryId
        ]);
    }

    public function delete($ledgerEntryId){
        $ledgerEntry = LedgerEntry::find($ledgerEntryId);
        $leaseId = $ledgerEntry->lease_id;
        $ledgerEntry->delete();
        Lease::find($leaseId)->setPaymentStatus();
        return redirect()->back()->with('message', 'Ledger entry deleted successfully.');
    }

    public function add()
    {
        $query = request()->all();
        $leaseId = $query['lease'] ?? false;
        $lease = $leases = null;
        if ($leaseId) {
            $lease = Lease::find($leaseId);
        } else {
            $leases = [];
            $leaseResults = (new Lease())->orderBy('display_name', 'asc')->get(['display_name','id']);
            foreach ($leaseResults as $leaseResult) {
                $leases[$leaseResult->id] = $leaseResult->display_name;
            }
        }

        if (!empty($query['add_ledger_entry']) && !empty($lease)) {
            $lease->addLedgerEntry(
                $query['type'],
                $query['category'],
                $query['amount'],
                $query['applied_date'],
                $query['description'],
                [    ]
            );
            $lease->setStatus()->setPaymentStatus();
            return redirect('/tenants/lease/'.$leaseId.'?tab=ledger')->with('message', 'Ledger entry added successfully.');
        }
        return view('ledger.addOrEdit', compact('leaseId', 'lease', 'leases'));
    }

    public function editLedgerEntry($ledgerEntryId)
    {
        $ledgerEntry = LedgerEntry::find($ledgerEntryId);
        $leaseId = $ledgerEntry->lease_id;
        $lease = Lease::find($leaseId);
        $post = request()->post();

        if(!empty($post)){
            $ledgerEntry->type = $post['type'];
            $ledgerEntry->category = $post['category'];
            $ledgerEntry->amount = $post['amount'];
            $ledgerEntry->applied_date = $post['applied_date'];
            $ledgerEntry->description = $post['description'];
            $ledgerEntry->save();
            Lease::find($leaseId)->setPaymentStatus();
            return redirect('/tenants/lease/'.$leaseId.'?tab=ledger')->with('message', 'Ledger entry updated successfully.');
        }
        
        return view('ledger.addOrEdit', compact('ledgerEntry', 'leaseId', 'lease'));
    }

    public function toggle($type, $ledgerEntryId){
        $ledgerEntry = LedgerEntry::find($ledgerEntryId);
        $field = ($type == 'flag') ? 'needs_attention' : 'is_ok';
        
        $newValue = ($ledgerEntry->$field == 1) ? 0 : 1;
        $ledgerEntry->$field = $newValue;
        $ledgerEntry->save();
        return response()->json([
            'status' => 'success',
            'value' => $newValue 
        ]);
    }



    
}
