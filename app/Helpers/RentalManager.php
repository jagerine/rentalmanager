<?php 
    namespace App\Helpers;

    class RentalManager { 

        /**
         * Format a number as currency and return it with symbol
         *
         * @param mixed $amount
         * @return string
         */
        public static function formatCurrency($amount) 
        {
            $prefix = '';
            if(floatval($amount) < 0) {
                $prefix = '-';
                $amount = abs($amount);
            }
            return $prefix . '$' . number_format($amount, 2);
        }

        public static function getDateTimeFormat(){
            return self::getDateFormat().' @ '.self::getTimeFormat();
        
        }
        public static function getDateFormat(){
            return 'M j, Y';
        
        }
        public static function getTimeFormat(){
            return 'h:i a';
        }
    }