<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaseTenant extends Model
{
    use HasFactory;
    public $fillable = ['lease_id', 'tenant_id', 'notes'];

    public function tenant()
    {
        return Tenant::find($this->tenant_id);
    }
    
}
