<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class HistoricalNote extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $fillable = ['note', 'object_id', 'object_type', 'created_by', 'updated_by', 'deleted_by'];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {     
            $model->created_by = $model->updated_by = Auth::id();
        });
        static::updating(function($model)
        {     
            $model->updated_by = Auth::id();
        });
        static::deleted(function($model)
        {     
            $model->deleted_by = Auth::id();
            $model->save();
        });
    }

    public static function getObjectNotes($type, $id){
        return self::leftJoin('users', 'users.id', '=', 'historical_notes.created_by')
                    ->select('historical_notes.id', 'historical_notes.note', 'historical_notes.created_at', 
                        'historical_notes.created_by', 'users.name')
                    ->where('historical_notes.object_type', $type)
                    ->where('historical_notes.object_id', $id)
                    ->orderBy('historical_notes.created_at', 'desc')
                    ->get();
    }

    public function createdBy(){
        return User::find($this->created_by);
    }

}
