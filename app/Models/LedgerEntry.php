<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\HistoricalNote;

class LedgerEntry extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['lease_id','type','category', 'applied_date', 'description', 'amount', 'is_ok', 'is_regular_rent', 
                        'auto_rent_charge', 'is_outstanding', 'needs_attention', 'custom_data'];

    public function historicalNotes()
    {
        return HistoricalNote::getObjectNotes('ledger', $this->id);
    }    
}
