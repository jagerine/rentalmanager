<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Lease;

class RentalUnit extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $fillable = ['name', 'address', 'unit', 'city', 'state', 'zip', 'status', 'current_lease_id'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            if (empty($builder->getQuery()->orders)) {
                $builder->orderBy('name', 'asc');
            }
        });
        static::creating(function ($model) {
            $model->created_by = $model->updated_by = Auth::id();
        });
        static::updating(function ($model) {
            $model->updated_by = Auth::id();
        });
        static::deleted(function ($model) {
            $model->deleted_by = Auth::id();
        });
    }

    public static function getSummary()
    {
        return self::leftJoin('leases', 'leases.id', '=', 'rental_units.current_lease_id')
                    ->select(
                        'rental_units.id',
                        'rental_units.name',
                        'rental_units.unit',
                        'rental_units.address',
                        'rental_units.city',
                        'rental_units.state',
                        'rental_units.zip',
                        'rental_units.status',
                        'leases.display_name',
                        'leases.rent',
                        'leases.per',
                        'leases.payment_status',
                        'leases.payment_status_description',
                        'rental_units.current_lease_id'
                    )
                    ->orderBy('rental_units.name')
                    ->get();
    }

    public function setStatus()
    {
        $activeLease = Lease::where('rental_unit_id', $this->id)
            ->where('status', 'Active')
            ->orderBy('id', 'desc')
            ->first();
        if ($activeLease) {
            $this->status = 'Occupied';
            $this->current_lease_id = $activeLease->id;
        } else {
            $this->status = 'Vacant';
            $this->current_lease_id = 0;
        }
        $this->save();
        return $this;
    }

    public static function getPostValidation()
    {
        return [
            'name' => "required|min:3|string",
            'address' => "nullable|min:3|string",
            'unit' => "nullable|string",
            'city' => "nullable|string",
            'state' => "nullable|string",
            'zip' => "nullable|string"
        ];
    }

    public static function getStatusCounts()
    {
        $return = [];
        $statuses = self::select('status')->distinct()->orderBy('status')->get();
        foreach ($statuses as $status) {
            $return[$status->status] = self::where('status', $status->status)->count();
        }
        return $return;
    }
}
