<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    use HasFactory;
    public $fillable = ['first_name', 'last_name', 'address', 
        'city', 'state', 'zip', 'phone', 'email', 'alt_phone', 'status', 
        'emergency_contact', 'description', 'searchable'];
}
