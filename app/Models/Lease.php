<?php

namespace App\Models;

use App\Helpers\RentalManager;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\Models\LedgerEntry;
use App\Models\RentalUnit;
use App\Models\LeaseTenant;

class Lease extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['rental_unit_id', 'display_name', 'start_date', 'end_date', 'rent', 'auto_rent_charge', 
        'deposit', 'due_on', 'status', 'mtm_continuous', 'per', 'description', 'payment_status', 'payment_status_description'];

    public static function boot()
    {
        parent::boot();
        static::creating(function($model)
        {     
            $model->created_by = $model->updated_by = Auth::id();
        });
        static::updating(function($model)
        {     
            $model->updated_by = Auth::id();
        });
        static::deleted(function($model)
        {     
            $model->deleted_by = Auth::id();
            $model->save();
        });
    }

    public static function getTableList()
    {
        $all = self::select('leases.id', 'display_name', 'name', 'rental_unit_id', 'start_date', 
                            'end_date', 'rent', 'per', 'due_on', 'deposit', 'payment_status')
                    ->join('rental_units', 'rental_units.id', '=', 'leases.rental_unit_id')
                    ->where('leases.status', 'Active')
                    ->orderBy('rental_units.name')
                    ->get();
        $headings = ['Unit', 'Tenant', 'Start Date', 'Rent', 'Status'];
        $rows = [];
        foreach ($all as $key => $value) {
            $tenants = LeaseTenant::join('tenants', 'tenants.id', '=', 'lease_tenants.tenant_id')
                ->where('lease_tenants.lease_id', $value->id)
                ->get();
            $rows[] = [
                '_actions' => [
                    (object) ['text' => 'Update Details', 'url' => '/tenants/edit/'.$value['id'], 'icon' => 'pencil-alt'],
                    (object) ['text' => 'End Lease', 'url' => '/tenants/lease-term/'.$value['id'], 'icon' => 'handshake'], 
                    (object) ['text' => 'Refresh Data', 'url' => '/tenants/refresh-lease-data/'.$value['id'], 'icon' => 'sync'], 
                ], 
                '_links' => [
                    'tenant' => '/tenants/lease/'.$value['id']
                ],
                'unit' => $value->name, 
                'tenant' => $value->display_name, 
                'start_date' => date('m/d/Y', strtotime($value->start_date)), 
                'rent' => $value->rent.'/'.self::perAbbreviation($value->per),
                'status' => $value->payment_status, 
            ];
        }
        return (object) [
            'headings' => $headings,
            'rows' => $rows
        ];
    }

    public static function perAbbreviation($per){
        switch ($per) {
            case 'month':
                return 'mo';
                break;
            case 'week':
                return 'wk';
                break;
            case 'day':
                return 'd';
                break;
            default:
                return '';
        }
    }

    public function rentalUnit()
    {
        $rentalUnit = RentalUnit::find($this->rental_unit_id);
        return $rentalUnit;
    }

    public function tenants()
    {  
        $leaseTenants = LeaseTenant::where('lease_id', $this->id)->get();
        $tenants = [];
        foreach ($leaseTenants as $leaseTenant) {
            $tenants[] = $leaseTenant->tenant();
        }
        return $tenants;
    }

    /**
     * setDisplayName 
     * 
     * Get the tenant names and save them to the lease for easy viewing
     * @return self::class
     */
    public function setDisplayName()
    {
        $tenants = LeaseTenant::join('tenants', 'tenants.id', '=', 'lease_tenants.tenant_id')
            ->where('lease_tenants.lease_id', $this->id)
            ->get();
        $display_name = [];
        foreach ($tenants as $value) {
            $display_name[] = $value->first_name . ' ' . $value->last_name;
        }
        $this->display_name = implode(', ', $display_name);
        $this->save();
        return $this;
    }

    /**
     * setPaymentStatus
     * 
     * Set the payment status of the lease
     * @return self::class
     */
    public function setPaymentStatus()
    {
        $ledger = $this->getLedger();
        if ($ledger['total'] == 0) {
            $this->payment_status = 'Current';
        } elseif ($ledger['total'] > 0) {
            $this->payment_status = 'Unpaid';
            $this->payment_status_description = json_encode($ledger['summary']);
        } else {
            $this->payment_status = 'Credit';
        }

        $this->save();
        return $this;
    }

    public function getLedger(){
        $now = time();
        $ledgerEntries = $this->getLedgerEntries();
        $categories = $pluses = $minuses = [];
        foreach ($ledgerEntries as $value) {
            if(!isset($categories[$value->category])){
                $categories[$value->category] = ['plus' => [], 'minus' => []];
            }
            if ($value->type == 'Credit') {
                $categories[$value->category]['plus'][] = $value->amount;
                $pluses[] = $value->amount;
            } else {
                $entryTime = strtotime($value->applied_date);
                if ($entryTime > $now) {
                    continue;
                }
                $categories[$value->category]['minus'][] = $value->amount;
                $minuses[] = $value->amount;
            }
        }

        $totalPlus = array_sum($pluses);
        $totalMinus = array_sum($minuses);
        $calculatedTotal = $totalMinus - $totalPlus;
        $summary = null;

        //If outstanding balance is detected, add it to the summary
        if($calculatedTotal > 0){
            $summary = [];
            foreach ($categories as $key => $value) {
                $plus = array_sum($value['plus']);
                $minus = array_sum($value['minus']);
                $catTotal = $plus - $minus;
                if($catTotal < 0){
                    $summary[$key] = RentalManager::formatCurrency($catTotal); 
                }
            }
        }
        
        return [
            'plus' => $totalPlus,
            'minus' => $totalMinus,
            'total' => $calculatedTotal,
            'rows' => $ledgerEntries, 
            'summary' => $summary
        ];
    }

    public function setStatus(){
        $this->status = 'Active';
        if(strtotime($this->end_date) < strtotime(date('Y-m-d'))){
            if (!$this->mtm_continuous) {
                $this->status = 'Inactive';
            }
        }
        $this->save();
        return $this;
    }

    public function getLedgerEntries(){
        return LedgerEntry::where('lease_id', $this->id)->orderBy('applied_date', 'desc')->get();
    }
    public function getNotes(){
        return HistoricalNote::where('object_type', 'lease')
            ->where('object_id', $this->id)
            ->orderBy('created_at', 'desc')->get();
    }

    public function addLedgerEntry($type, $category, $amount, $date, $description = null, $additionalOptions = []){
        if (!$date) {
            $date = date('Y-m-d');
        }
        $ledgerEntry = new LedgerEntry;
        $ledgerEntry->type = $type;
        $ledgerEntry->category = $category;
        $ledgerEntry->amount = $amount;
        $ledgerEntry->description = $description;
        $ledgerEntry->applied_date = $date;
        $ledgerEntry->lease_id = $this->id;

        foreach($additionalOptions as $key => $value){
            $ledgerEntry->{$key} = $value;
        }
        $ledgerEntry->save();
        return $ledgerEntry;
    }

    public function getNextDueDate($time = false)
    {
        $nextDueDate = false;
        if($time === false){
            $time = time();
        }
        if($this->per == 'month'){
            $thisMonth = $dueMonth = date('n', $time);
            $thisDate = date('d', $time);
            $thisYear = $dueYear = date('Y', $time);
            $dueOn = $this->due_on;
            if($this->due_on == 'First'){
                $dueOn = '1';
            }
            if($this->due_on == 'Last'){
                $dueOn = date('t', strtotime($thisMonth.'/01/'.$thisYear));
            }

            if((int) $dueOn < (int) $thisDate || $dueOn == $thisDate){
                $dueMonth++;
                if($dueMonth > 12){
                    $dueMonth = '1';
                    $dueYear++;
                }
            } 
            $nextDueDate = $dueYear.'-' . $dueMonth . '-' . $dueOn;
        }

        if($this->per == 'week'){
            $nextDueDate = date('Y-m-d', strtotime('next .'.$this->due_on));
        }

        if($this->per == 'day'){
            $nextDueDate = date('Y-m-d', strtotime('tomorrow'));
        }

        return $nextDueDate;
    }

    public function setNextDueDate($time = false)
    {
        $this->next_due_date = $this->getNextDueDate($time);
        $this->save();
        return $this;
    }

    public function getPossibleCharges($time)
    {
        $now = time();
        if($time === false){
            $time = $now;
        }
        $return = [];
        $nextDue = $return[] = $this->getNextDueDate($time);
        $count = 0;
        while(strtotime($nextDue) < $now && $count < 60){
            $time = strtotime($nextDue);
            $nextDue = $this->getNextDueDate($time);
            $return[] = $nextDue;
            $count++;
        }
        return $return;
    }

    public function checkForRentCharge($time = false)
    {
        if($time === false){
            $time = time();
        }
        $today = (int) date('d', $time);

        dd($today); 
        if($today == $this->due_on){
            $this->addLedgerEntry('Debt', 'Rent' , $this->rent, date('Y-m-d', $time), 'Rent Due', ['auto_rent_charge' => 1]);
        }
        $this->setPaymentStatus();
    }

}
