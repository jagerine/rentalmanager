<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLedgers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledger_entries', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('lease_id')->unsigned();
            $table->string('type')->nullable();
            $table->string('category')->nullable();
            $table->date('applied_date')->nullable();
            $table->string('description')->nullable();
            $table->float('amount')->nullable();
            $table->boolean('is_ok')->default(false);
            $table->boolean('is_outstanding')->default(false);
            $table->boolean('needs_attention')->default(false);
            $table->boolean('auto_rent_charge')->default(false);
            $table->boolean('is_regular_rent')->default(false);
            $table->longText('custom_data')->nullable();

            $table->softDeletes();
            $table->timestamps();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->bigInteger('deleted_by')->unsigned()->nullable();

            $table->foreign('lease_id')->references('id')->on('leases');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('deleted_by')->references('id')->on('users');
            $table->index('deleted_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ledger_entries');
    }
}
