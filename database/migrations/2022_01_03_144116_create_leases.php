<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leases', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rental_unit_id')->unsigned()->nullable();
            $table->string('display_name')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->date('term_date')->nullable();
            $table->boolean('mtm_continuous')->nullable();
            $table->decimal('deposit', 10, 2)->nullable();
            $table->decimal('rent', 10, 2)->nullable();
            $table->string('per')->nullable()->default('month');
            $table->char('due_on')->nullable();
            $table->date('next_due_date')->nullable();
            $table->string('status')->nullable();
            $table->string('payment_status')->nullable();
            $table->longText('payment_status_description')->nullable();
            $table->longText('description')->nullable();
            
            $table->softDeletes();
            $table->timestamps();
            $table->bigInteger('created_by')->unsigned()->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();
            $table->bigInteger('deleted_by')->unsigned()->nullable();

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->foreign('deleted_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leases');
    }
}
